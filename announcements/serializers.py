from .models import Announcement
from rest_framework import serializers

'''
Announcement Serializer
Serializes announcement fields
'''
class AnnouncementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Announcement
        fields = ('header', 'content', 'type')
