from django.db import models

TYPE = (('blue', 'Info'),
        ('yellow', 'Warning'),
        ('red', 'Danger'),
        )

class Announcement(models.Model):
    header = models.CharField(max_length=100)
    content = models.CharField(max_length=100)
    type = models.CharField(max_length=10, choices=TYPE, default='blue')

    def __str__(self):
        return self.header