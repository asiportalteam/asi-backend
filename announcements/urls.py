from django.conf.urls import url
from announcements import views

urlpatterns = [
    url(r'^list/', views.AnnouncementList.as_view()),
]
