from django.contrib import admin
from .models import Announcement

class AnnouncementAdmin(admin.ModelAdmin):
    fields = ('header', 'content', 'type')
    list_display = ('header', 'content', 'type')
    list_filter = ('type',)

admin.site.register(Announcement, AnnouncementAdmin)
