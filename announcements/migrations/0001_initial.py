# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2021-10-05 04:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Announcement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('header', models.CharField(max_length=100)),
                ('content', models.CharField(max_length=100)),
                ('type', models.CharField(choices=[('blue', 'Info'), ('yellow', 'Warning'), ('red', 'Danger')], default='blue', max_length=10)),
            ],
        ),
    ]
