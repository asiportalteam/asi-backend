from .models import Announcement
from .serializers import AnnouncementSerializer
from rest_framework import generics, status
from rest_framework.response import Response


class AnnouncementList(generics.GenericAPIView):
    '''
    AnnouncementList
    Simple list view. Will just grab all announcements.
    '''
    serializer_class = AnnouncementSerializer

    def get(self, request):
        qs = Announcement.objects.all()
        content = self.serializer_class(qs, many=True)
        return Response(content.data, status=status.HTTP_200_OK)
