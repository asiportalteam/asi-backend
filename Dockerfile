# The magical version of python that can run our project without bricking
FROM python:3.6.5
ENV PYTHONUNBUFFERED=1
# Name of folder that will house our code on the container
WORKDIR /backend
# Install all of our packages
COPY requirements.txt /backend/
RUN pip install -r requirements.txt
COPY . /backend/
EXPOSE 8000