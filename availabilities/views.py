from rest_framework import permissions
from rest_framework.response import Response
from custom_permissions import permissions as cp
from custom_users.models import User
from .models import Availability
from .serializers import AvailabilitySerializer, UpdateAvailabilitySerializer
import django_filters.rest_framework
from rest_framework import generics, status
from rest_framework.views import APIView


class AvailabilityListAllView(generics.GenericAPIView):
    '''
    Endpoint to get all the Availability
    '''
    queryset = Availability.objects.all()
    serializer_class = AvailabilitySerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('ambassador', 'start_time',
                     'is_scheduled', 'is_scheduleable', 'day')

    def get(self, request):
        valid_users = User.objects.filter(
            is_tutor=True,
            courses_can_tutor__pk=pk,
        )
        over = [user for user in valid_users if user.is_overtime()]
        qs = self.queryset.filter(
            is_scheduled=False,
            can_schedule=True,
            ambassador__in=valid_users,
        ).exclude(ambassador__in=over, ambassador=request.user)
        if qs.count() > 0:
            content = self.serializer_class(qs, many=True)
            return Response(content.data, status=status.HTTP_200_OK)
        else:
            content = {}
            return Response(content, status=status.HTTP_204_NO_CONTENT)


class AvailabilityListView(generics.GenericAPIView):
    '''
    Endpoint to get Availability by course
    '''
    queryset = Availability.objects.all()
    serializer_class = AvailabilitySerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('ambassador', 'start_time',
                     'is_scheduled', 'is_scheduleable', 'day')

    def get(self, request, pk):
        valid_users = User.objects.filter(
            is_tutor=True,
            courses_can_tutor__pk=pk,
        )
        over = [user for user in valid_users if user.is_overtime()]
        qs = self.queryset.filter(
            is_scheduled=False,
            can_schedule=True,
            ambassador__in=valid_users,
        ).exclude(ambassador__in=over)
        if qs.count() > 0:
            content = self.serializer_class(qs, many=True)
            return Response(content.data, status=status.HTTP_200_OK)
        else:
            content = {}
            return Response(content, status=status.HTTP_204_NO_CONTENT)


class AmbassadorAvailabilityListView(generics.GenericAPIView):
    '''
    Endpoint for an ambassador to see their Availability 
    '''
    queryset = Availability.objects.all()
    serializer_class = AvailabilitySerializer

    def get(self, request):
        if request.user.is_tutor:
            qs = self.queryset.filter(
                ambassador=request.user).order_by('start_time')
            if qs.count() > 0:
                content = self.serializer_class(qs, many=True)
                return Response(content.data, status=status.HTTP_200_OK)
            else:
                return Response({'errors': ['You have not had availabilities made yet. Please contact your team leader for assistance.']}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'errors': ['You are not a tutor.']}, status=status.HTTP_401_UNAUTHORIZED)


class AvailabilityUpdateManyView(generics.GenericAPIView):
    queryset = Availability.objects.all()
    serializer_class = UpdateAvailabilitySerializer

    def patch(self, request):
        content = list()
        for item in request.data.get('data'):
            ava = Availability.objects.get(id=item['id'])
            serializer = self.serializer_class(ava, data=item)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            content.append(serializer.data)
        qs = self.queryset.filter(ambassador=request.user)
        content = AvailabilitySerializer(qs, many=True)
        return Response(content.data, status=status.HTTP_200_OK)


class AvailabilityUpdateView(generics.GenericAPIView):
    queryset = Availability.objects.all()
    serializer_class = UpdateAvailabilitySerializer

    def patch(self, request):
        ava = Availability.objects.get(id=pk)
        serializer = self.serializer_class(ava, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)


class AvailabilityCreatorView(generics.GenericAPIView):
    '''
    Endpoint only accessable by team leaders.
    '''
    permission_classes = (permissions.IsAuthenticated, cp.IsTeamLeader)
    serializer_class = AvailabilitySerializer

    def post(self, request, pk):
        '''
        Take a request and a pk and just check dude.
        '''
        user = User.objects.get(pk=pk)
        if user.is_tutor and Availability.objects.filter(ambassador=user).count() == 0:
            Availability.make_availabilities(user)
            content = AvailabilitySerializer(
                Availability.objects.filter(ambassador=user), many=True)
            return Response({'msg': 'Successfully created availabilites for {0}'.format(user.get_full_name())}, status=status.HTTP_201_CREATED)
        else:
            content = {
                'errors': 'User is not an ambassador or User already has availabilities.',
            }
            return Response(content, status=status.HTTP_400_BAD_REQUEST)
