from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .models import Availability
from custom_users.models import User


class AmbassadorOnlyFilter(admin.SimpleListFilter):
    title = _('Ambassadors')
    parameter_name = 'ambassador'

    def lookups(self, request, model_admin):
        qs = User.objects.filter(is_tutor=True).exclude(email='admin@fiu.edu')
        return (
            (user.id, _(user.get_full_name())) for user in qs)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(ambassador=self.value())
        else:
            return queryset


class AvailabilityAdmin(admin.ModelAdmin):
    fields = ('ambassador', 'start_time', 'day',
              'is_scheduled', 'can_schedule')
    list_display = ('get_ambassador_name', 'start_time', 'day',
                    'is_scheduled', 'can_schedule')
    list_filter = (AmbassadorOnlyFilter, 'day', 'is_scheduled',
                   'can_schedule', 'start_time')

    def get_ambassador_name(self, obj):
        return obj.ambassador.get_full_name()
    get_ambassador_name.short_description = "Ambassador"


admin.site.register(Availability, AvailabilityAdmin)
