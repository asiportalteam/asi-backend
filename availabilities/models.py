from asi_api.globals import EARLIEST_TIME, LATEST_TIME
from custom_users.models import User
from django.utils import timezone
from django.db import models
from datetime import time, timedelta, datetime

DAYS = (
    (1, 'Monday'),
    (2, 'Tuesday'),
    (3, 'Wednesday'),
    (4, 'Thursday'),
    (5, 'Friday'),
)


class Availability(models.Model):
    '''
    Availability
    Models when a tutor would be available for tutoring.

    - ambassador : A foreignkey to the user model
    - start_time : The start time for the availability
    - day : Day of the week
    - is_scheduled : Whether this time has been requested or is scheduled
    - can_schedule : Whether the ambassador can actually obtain this time
    '''
    ambassador = models.ForeignKey(
        User,
        related_name='availabilities'
    )
    start_time = models.TimeField()  # we know that they're an hour long
    day = models.IntegerField(choices=DAYS, default=1)
    is_scheduled = models.BooleanField(default=False)
    can_schedule = models.BooleanField(default=False)

    '''
    Create all availabilities for a given user
    '''
    @staticmethod
    def make_availabilities(user):
        avas = []
        for actual, display in DAYS:
            for i in range(11, 21):  # we're gonna go from 11 am - 8 pm
                stime = time(i, 0, 0)
                dy = actual
                avas += [Availability.objects.create(
                    start_time=stime,
                    day=dy,
                    ambassador=user)]
        return avas

    def get_american_time(self):
        '''
        'merica best time format
        F U C K 24 hour time
        '''
        return self.start_time.strftime('%I:%M %p')

    def get_anonymous_time(self):
        start = "{}".format(
                self.start_time.strftime("%I:%M %p"),
        )

        end = self.get_end_time()
        return "{} - {} on {}".format(
            start,
            end,
            self.get_day_display(),
        )

    def get_end_time(self):
        '''
        this is so ugly lmao
        Why is it so hard to add 1 hour to time in python??
        '''
        return (datetime.combine(timezone.now().date(), self.start_time) + timedelta(hours=1)).time().strftime('%I:%M %p')

    def __str__(self):
        start = "{}".format(
                self.start_time.strftime(":%M %p"),
        )
        end = self.get_end_time()
        return "{} from {}-{} on {}".format(
            self.ambassador.get_full_name(),
            start,
            end,
            self.get_day_display(),
        )
