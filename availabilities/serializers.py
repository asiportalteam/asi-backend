from rest_framework.response import Response
from rest_framework import serializers
from .models import Availability

class RequestAvailabilitySerializer(serializers.ModelSerializer):
    start_time = serializers.ReadOnlyField(source='get_american_time')
    end_time = serializers.SerializerMethodField()

    def get_end_time(self, obj):
        return obj.get_end_time()

    class Meta:
        model = Availability
        fields = ('id', 'ambassador', 'start_time', 'end_time')
        ''' 
        None of this shouuld  be changed by the end user.
        All of this would be modified by the actual application
        '''
        extra_kwargs = {
                'id' : {'read_only' : True},
                'ambassador' : {'read_only' : True},
                'start_time' : {'read_only' : True},
                }

class AvailabilitySerializer(serializers.ModelSerializer):
    start_time = serializers.ReadOnlyField(source='get_american_time')
    class Meta:
        model = Availability
        fields = ('id', 'ambassador', 'start_time', 'is_scheduled', 'can_schedule', 'day')
        ''' 
        None of this shouuld  be changed by the end user.
        All of this would be modified by the actual application
        '''
        extra_kwargs = {
                'id' : {'read_only' : True},
                'ambassador' : {'read_only' : True},
                'start_time' : {'read_only' : True},
                'is_scheduled' : { 'read_only' : True},
                'day' : {'read_only' : True},
                }

class UpdateAvailabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Availability
        fields = ('id', 'can_schedule')
        ''' 
        None of this shouuld  be changed by the end user.
        All of this would be modified by the actual application
        '''
        read_only_fields = ('id',)
