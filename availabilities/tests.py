from rest_framework.test import APITestCase
from courses.models import Course
from django.test import TestCase
from django.urls import reverse
from custom_users.models import User
from asi_api.globals import EARLIEST_TIME, LATEST_TIME, NUM_DAYS
from .models import Availability


class CommonSetUp(TestCase):
    def setUp(self):
        self.ambassador = User.objects.create(
            first_name='Jake',
            last_name='Lopez',
            email='jlope590@fiu.edu',
            password='t35tp455w0rd',
            is_tutor=True,
        )


class TestAvailabilityCreation(CommonSetUp):
    def test_that_availabilities_are_created(self):
        Availability.make_availabilities(self.ambassador)
        self.assertEqual(Availability.objects.all().count(),
                         (abs(LATEST_TIME - EARLIEST_TIME) * NUM_DAYS))


class TestAvailabilityListView(CommonSetUp, APITestCase):
    ''' 
    Gonna test a few things
    '''

    def setUp(self):
        self.course = Course.objects.create(
            subject='MAC',
            number='2312',
        )
        self.user = User.objects.create(
            username='joblow@fiu.edu',
            email='joblow@fiu.edu',
            first_name='Joe',
            last_name='Blow',
        )
        self.jwt_token = self.user.token
        super(TestAvailabilityListView, self).setUp()

    def test_that_no_availabilities_gives_a_204(self):
        '''
        Ambassador has no courses they can tutor. so there should be no availabilities returned.
        '''
        url = reverse('availabilities:list', kwargs={'pk': 1})
        self.client.credentials(
            HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 204)

    def test_that_only_tutors_who_can_tutor_course_are_shown(self):
        Availability.make_availabilities(self.ambassador)
        url = reverse('availabilities:list', kwargs={'pk': 1})
        self.client.credentials(
            HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 204)
        self.ambassador.courses_can_tutor.add(self.course)
        self.ambassador.save()
        avas = Availability.objects.filter(ambassador=self.ambassador)
        for ava in avas:
            ava.can_schedule = True
            ava.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_that_only_tutors_not_over_time_are_shown(self):
        self.ambassador.session_limit = 0
        self.ambassador.save()
        self.ambassador.courses_can_tutor.add(self.course)
        Availability.make_availabilities(self.ambassador)
        url = reverse('availabilities:list', kwargs={'pk': self.course.pk})
        self.client.credentials(
            HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 204)
        self.ambassador.courses_can_tutor.add(self.course)
        self.ambassador.save()
        avas = Availability.objects.filter(ambassador=self.ambassador)
        for ava in avas:
            ava.can_schedule = True
            ava.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 204)
