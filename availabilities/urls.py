from django.conf.urls import url
from availabilities import views

'''
    Need 2 more end points:
        1. availability for the user
        2. availability for a course
'''
urlpatterns = [
        url(r'^list/(?P<pk>[0-9]+)$', views.AvailabilityListView.as_view(), name='list'),
        url(r'^list/all$', views.AvailabilityListAllView.as_view(), name="everything"),
        url(r'^list/ambassador$', views.AmbassadorAvailabilityListView.as_view(), name='amb_list'),
        url(r'^update/(?P<pk>[0-9]+)$', views.AvailabilityUpdateView.as_view()),
        url(r'^update/many$', views.AvailabilityUpdateManyView.as_view()),
        url(r'^create/(?P<pk>[0-9]+)$', views.AvailabilityCreatorView.as_view()),
    ]
