# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-15 22:36
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='TutoringSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('start_time', models.TimeField()),
                ('session_canceled', models.BooleanField(default=False)),
                ('ambassador', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ambassador_tutoring_sessions', to=settings.AUTH_USER_MODEL)),
                ('tutee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tutee_tutoring_sessions', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
