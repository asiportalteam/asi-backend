from datetime import timedelta, datetime
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.utils import timezone
from custom_permissions.permissions import IsTeamLeader
import django_filters.rest_framework
from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework.views import APIView
from availabilities.models import Availability
from rest_framework.permissions import IsAuthenticated
from emailer.mailer import send_reminder
from surveys.models import AmbassadorSurvey, TuteeSurvey
from .serializers import (ScheduleViewSerializer, SessionCreationSerializer, WeeklyScheduleSerializer, SessionSerializer)
from .models import TutoringSession


class ScheduleView(APIView):
    '''
    Grabs a user and retrieves their sessions for the next week ( not including week days)
    Right now it only retrieves data for ambassadors. Will add the Q thing later.

    Returns a bunch of JSON objects of the form:
        {
            'date' : date,
            'sessions' : [a list of sessions ordered by time]'
        }
    '''

    def get(self, request):
        right_now = timezone.now()
        up_to = timezone.now() + timedelta(days=7)
        user = request.user
        # what a fuckin mouthful
        # this should be its own dman method lmao
        dates = [(timezone.now().date() + timedelta(days=x)) for x in range(7)
                 if (timezone.now().date() + timedelta(days=x)).isoweekday() not in [6, 7]]
        sessions = TutoringSession.objects.filter(
            Q(ambassador=user) | Q(tutee=user)
        )
        content = []
        for date in dates:
            content += [{
                'date': date.strftime('%A, %B %d, %Y'),
                'sessions':
                    list(ScheduleViewSerializer(
                        sessions.filter(day=date.isoweekday(), session_canceled = False,
                                        end_date__gte=date,
                                        start_date__lte=date).order_by('start_time'),

                        many=True, context={'request': request}).data)
            }]
        return Response(content, status=status.HTTP_200_OK)


class CreateSessionView(APIView):
    '''
    End point for LD's & TL's to create a manual session entry.
    '''
    permission_classes = (IsAuthenticated, IsTeamLeader)

    def post(self, request):
        serialzed = SessionCreationSerializer(data=request.data)
        serialzed.is_valid(raise_exception=True)
        session = serialzed.save()

        session = TutoringSession.objects.get(id=session.id)
        session.day = session.start_date.isoweekday()
        session.save()
        # if the availability does not exist i.e. it's some weird time like 9:30 a.m. then
        # this will throw an exception yelling about how the query does not exist.
        # So what we'll do is catch that exception and continue on
        # Probably better to use filter in this case and check the length but that would be too smart.
        try:
            availability = Availability.objects.get(
                ambassador=session.ambassador,
                start_time=session.start_time,
                day=session.day
            )
            if timezone.now().date() < session.end_date:
                availability.is_scheduled = True
                availability.save()
        except ObjectDoesNotExist:
            print("Must be for a non existent availability. Continue on")
        session.save()
        AmbassadorSurvey.create_from_session(session)
        TuteeSurvey.create_from_session(session)
        return Response({"msg": "We did it boys", "_dbug": serialzed.data})


class SessionsInRangeView(APIView):
    '''
    End point to gather sessions that fall within a range. Pretty much only useful for timesheet stuff.
    ---
    get:
        parameters_strategy:
            form: replace
        parameters:
            - name: start_date
              type: date

    '''
    permission_classes = (IsAuthenticated,)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    queryset = TutoringSession.objects.all()

    def get(self, request):
        user = request.user
        content = list()
        if(request.query_params):
            start_date = request.query_params.get('start_date')
            end_date = request.query_params.get('end_date')
            qs = self.queryset.filter(
                ambassador=user, start_date__lte=end_date, end_date__gte=start_date)
            start = datetime.strptime(start_date, '%Y-%m-%d').date()
            end = datetime.strptime(end_date, '%Y-%m-%d').date()
            for q in qs:
                dates = list(filter(lambda x: end > x > start, q.get_dates()))
                content += [{'session': q.timesheet_display,
                             'id': q.id,
                             'dates': dates}]
            return Response(content, status=status.HTTP_200_OK)


class SessionsOnDateView(APIView):
    permission_classes = (IsAuthenticated,)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    queryset = TutoringSession.objects.all()

    def get(self, request):
        user = request.user
        content = list()
        if (request.query_params):
            session_date = request.query_params.get('session_date')
            day = datetime.strptime(session_date, '%Y-%m-%d').isoweekday()
            qs = self.queryset.filter(
                ambassador=user, start_date__lte=session_date, end_date__gte=session_date,
                day=day
            )
            if qs:
                for q in qs:
                    content += [{'text': q.timesheet_display,
                                 'value': q.id,
                                 'date': session_date}]
                return Response(content, status=status.HTTP_200_OK)
            else:
                return Response({'msg': "Sorry we couldn't find anything on that date"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'msg': 'Not how this endpoint works.'}, status=status.HTTP_400_BAD_REQUEST)

class MyScheduledSessionsView(APIView):
    permission_classes = (IsAuthenticated, )
    queryset = TutoringSession.objects.all()

    def get(self, request):
        user = request.user
        qs = self.queryset.filter((Q(ambassador = user) | Q(tutee=user)), session_canceled=False, end_date__gte=timezone.now()).order_by('day', 'start_time')
        serialized = SessionSerializer(qs, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)



class ScheduledSessionsView(APIView):
    permission_classes = (IsAuthenticated, )
    queryset = TutoringSession.objects.all()

    def get(self, request):
        qs = self.queryset.filter(session_canceled=False).order_by('day', 'start_time')
        serialized = WeeklyScheduleSerializer(qs, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)

class EmailReminderView(APIView):
    permission_classes = (IsAuthenticated, )
    queryset = TutoringSession.objects.all()

    def get(self, request, id):
        session = self.queryset.filter(id=id)
        if len(session) == 1 and session[0].ambassador.email == request.user.email:
            send_reminder(session[0])
            message = {
                "message": "Reminder sent"
            }
            return Response(message, status=status.HTTP_200_OK)
        else:
            message = {
                "message": "Session is not found"
            }
            return Response(message, status=status.HTTP_400_BAD_REQUEST)
