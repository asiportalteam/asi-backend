from django.test import TestCase
from datetime import timedelta
from django.urls import reverse
from django.utils import timezone
from unittest import mock
from custom_users.models import User
from asi_api.globals import MIN_DAYS_TO_SCHEDULE
from courses.models import Course
from availabilities.models import Availability
from .models import TutoringSession
from rest_framework.test import APITestCase

import datetime


class TutoringSessionModelTest(TestCase):
    '''
    Test methods from within the tutoring session model
    '''

    def setUp(self):
        self.ts = TutoringSession.objects.create(
            ambassador=User.objects.create(
                username='tutor@fiu.edu',
                email='tutor@fiu.edu',
                is_tutor=True,
            ),
            tutee=User.objects.create(
                username='tutee@fiu.edu',
                email='tutee@fiu.edu',
                is_tutor=True,
            ),
            start_date=datetime.date(2017, 7, 3),
            course=Course.objects.create(
                subject='MAC',
                number='2311',
                full_name='Calculus Twooo',
                professor='Professor Fuckhead',
            ),
            end_date=datetime.date(2017, 7, 20),
            day=1,
            start_time=datetime.time(11, 0, 0),
        )

    def test_that_dates_returned_are_corect(self):
        '''
        Ensure that the dates we get back are what we expect
        '''
        dates = [datetime.date(2017, 7, 3), datetime.date(
            2017, 7, 10), datetime.date(2017, 7, 17)]
        self.assertEqual(self.ts.get_dates(), dates)

    def test_that_start_and_end_times_are_calculated_correctly(self):
        # December 2017
        # Su Mo Tu We Th Fr Sa
        #                 1  2
        # 3  4  5  6  7  8  9
        # 10 11 12 13 14 15 16
        # 17 18 19 20 21 22 23
        # 24 25 26 27 28 29 30
        # 31
        # so here's the scenario:
        # Sessions should take place wednesday
        # Session is scheduled on monday within 48 hours. First session should happen that wednesday
        scheduled_date = datetime.date(2017, 12, 11)
        what_i_thought = (datetime.date(2017, 12, 13),
                          datetime.date(2017, 12, 27))
        # such cheeky variable names
        what_i_got = TutoringSession.get_start_and_end_date(
            what_i_thought[0].isoweekday(),
            scheduled_date,
            end_date=datetime.date(2017, 12, 29)
        )
       # self.assertEqual(what_i_got, what_i_thought)

    def test_why_this_fucks_up_for_this(self):
        # December 2017
        # Su Mo Tu We Th Fr Sa
        #                 1  2
        # 3  4  5  6  7  8  9
        # 10 11 12 13 14 15 16
        # 17 18 19 20 21 22 23
        # 24 25 26 27 28 29 30
        # 31
        # so here's the scenario:
        # Sessions should take place wednesday
        # Session is scheduled on monday within 48 hours. First session should happen that wednesday
        scheduled_date = datetime.date(2017, 1, 17)
        what_i_thought = (datetime.date(2017, 1, 25),
                          datetime.date(2017, 4, 26))
        # such cheeky variable names

        what_i_got = TutoringSession.get_start_and_end_date(
            what_i_thought[0].isoweekday(),
            scheduled_date,
            end_date=datetime.date(2017, 4, 27)
        )
       # self.assertEqual(what_i_got, what_i_thought)


class CreateTutoringSessionAPITestCase(APITestCase):
    def setUp(self):
        self.course = Course.objects.create(
            subject='MAC',
            number='2312',
        )
        self.amb = User.objects.create(
            username='somedude@fiu.edu',
            email='somedude@fiu.edu',
            first_name='Some',
            last_name='Dude',
            is_logistic_director=True,
            is_team_leader=True,
            is_tutor=True,
        )
        self.amb.courses_can_tutor.add(self.course)
        self.amb.save()
        Availability.make_availabilities(self.amb)
        self.user = User.objects.create(
            username='joblow@fiu.edu',
            email='joblow@fiu.edu',
            first_name='Joe',
            last_name='Blow',
        )
        self.jwt_token = self.user.token

    def test_that_sessions_created_availability_not_shown(self):
        url = reverse('tutoring_sessions:create')
        # mark availability as schedulable
        ava = Availability.objects.all()[0]
        ava.can_schedule = True
        ava.save()
        availability = reverse('courses:available_times')
        self.client.credentials(
            HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.get(availability, format='json')
        old_avas = response.data
        self.client.credentials(
            HTTP_AUTHORIZATION='jwt {0}'.format(self.amb.token))

        date_to_use = timezone.now()
        days_off = ava.day - date_to_use.isoweekday()
        if date_to_use.isoweekday() > 5:
            date_to_use = date_to_use + timedelta(days=days_off)
        response = self.client.post(
            url, {
                'start_time': ava.start_time,
                'tutee': self.user.id,
                'ambassador': self.amb.id,
                'course': self.course.id,
                'start_date': date_to_use.strftime('%Y-%m-%d'),
                'end_date': (date_to_use + timedelta(days=30)).strftime('%Y-%m-%d')
            },
            format='json')
        self.assertEqual(response.status_code, 200)
        new_avas = self.client.get(availability, format='json').data
        self.assertNotEqual(new_avas, old_avas)

    def test_that_single_sessions_still_show_availability(self):
        url = reverse('tutoring_sessions:create')
        # mark availability as schedulable
        ava = Availability.objects.all()[0]
        ava.can_schedule = True
        ava.save()
        availability = reverse('courses:available_times')
        self.client.credentials(
            HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.get(availability, format='json')
        old_avas = response.data
        self.client.credentials(
            HTTP_AUTHORIZATION='jwt {0}'.format(self.amb.token))

        date_to_use = timezone.now()
        days_off = ava.day - date_to_use.isoweekday()
        if date_to_use.isoweekday() > 5:
            date_to_use = date_to_use + timedelta(days=days_off)
        response = self.client.post(
            url, {
                'start_time': ava.start_time,
                'tutee': self.user.id,
                'ambassador': self.amb.id,
                'course': self.course.id,
                'start_date': date_to_use.strftime('%Y-%m-%d'),
                'end_date': (date_to_use + timedelta(days=1)).strftime('%Y-%m-%d')
            },
            format='json')
        self.assertEqual(response.status_code, 200)
        new_avas = self.client.get(availability, format='json').data
        self.assertEqual(new_avas, old_avas)
