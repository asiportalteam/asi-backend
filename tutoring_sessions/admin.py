from django.contrib import admin
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from courses.models import Course
from custom_users.models import User
from surveys.models import AmbassadorSurvey, TuteeSurvey
from availabilities.models import Availability
from .models import TutoringSession


def cancel_sessions(modeladmin, request, queryset):
    queryset.update(session_canceled=True, end_date=timezone.now())
    for session in queryset:
        AmbassadorSurvey.objects.filter(tutoring_session=session,
                                        session_date__gte=timezone.now()).delete()
        TuteeSurvey.objects.filter(tutoring_session=session,
                                   session_date__gte=timezone.now()).delete()
        try:
            ava = Availability.objects.get(ambassador = session.ambassador,
                                        day = session.day,
                                        start_time = session.start_time)
            ava.is_scheduled = False
            ava.can_schedule = True
            ava.save()
        except ObjectDoesNotExist:
            print("Couldn't find related availability. Must not exist??")


cancel_sessions.short_description = "Mark sessions as canceled and Delete Surveys"


class AmbassadorOnlyFilter(admin.SimpleListFilter):
    title = _('Ambassadors')
    parameter_name = 'ambassador'

    def lookups(self, request, model_admin):
        qs = User.objects.filter(is_tutor=True)
        return (
            (user.id, _(user.get_full_name())) for user in qs)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(ambassador=self.value())
        else:
            return queryset


class OfferedCourseOnlyFilter(admin.SimpleListFilter):
    title = _('course')
    parameter_name = 'course'

    def lookups(self, request, model_admin):
        courses = Course.objects.filter(offered=True)
        return (
            (course.id, _(course.subject+course.number)) for course in courses)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(course=self.value())
        else:
            return queryset


class TutoringSessionAdmin(admin.ModelAdmin):
    fields = ('day', 'ambassador', 'tutee', 'course',
              'start_time', 'start_date', 'end_date', 'session_canceled')
    list_display = ('day', 'course', 'start_time', 'tutee',
                    'ambassador',  'start_date', 'end_date', 'session_canceled')
    list_filter = (AmbassadorOnlyFilter, OfferedCourseOnlyFilter, 'day', 'session_canceled')
    actions = [cancel_sessions]

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


admin.site.register(TutoringSession, TutoringSessionAdmin)
