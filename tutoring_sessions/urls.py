from django.conf.urls import url
from tutoring_sessions import views

urlpatterns = [
    url(r'^weekly-schedule/', views.ScheduleView.as_view()),
    url(r'^scheduled-sessions/', views.ScheduledSessionsView.as_view()),
    url(r'^create/', views.CreateSessionView.as_view(), name="create"),
    url(r'^in-range/?$', views.SessionsInRangeView.as_view()),
    url(r'^on-date/?$', views.SessionsOnDateView.as_view()),
    url(r'^my-sessions/$', views.MyScheduledSessionsView.as_view()),
    url(r'^send-reminder/(?P<id>[0-9]+)$', views.EmailReminderView.as_view())
]
