from rest_framework.response import Response
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import TutoringSession
from datetime import datetime, timedelta
from django.utils import timezone


class ScheduleViewSerializer(ModelSerializer):
    course = serializers.ReadOnlyField(source='course.__str__')
    tutee = serializers.ReadOnlyField(source='tutee.get_full_name')
    tutee_email = serializers.ReadOnlyField(source='tutee.email')
    ambassador = serializers.ReadOnlyField(source='ambassador.get_full_name')
    ambassador_email = serializers.ReadOnlyField(source='ambassador.email')
    day = serializers.ReadOnlyField(source='get_day_display')
    start_time = serializers.ReadOnlyField(source='get_american_time')
    session_type = serializers.SerializerMethodField()
    display_link = serializers.SerializerMethodField()
    zoom_pmi = serializers.ReadOnlyField(source='ambassador.zoom_pmi')

    def get_session_type(self, obj):
        if obj.tutee == self.context['request'].user:
            return 'tutee'
        else:
            return 'ambassador'

    # Method to determine whether the Zoom link is displayed in the dashboard
    # Returns true if the current day of the week matches and the current time is before the end_time
    @staticmethod
    def get_display_link(obj):
        current_time = timezone.localtime()
        end_time = timezone.make_aware(datetime.combine(current_time.date(), obj.start_time) + timedelta(hours=1))

        if obj.day == current_time.date().isoweekday() and current_time <= end_time:
            return True
        else:
            return False

    class Meta:
        model = TutoringSession
        fields = ('start_time', 'tutee', 'ambassador',
                  'day', 'course', 'session_canceled', 'session_type', 'display_link',
                  'tutee_email', 'ambassador_email', 'zoom_pmi', 'id')
        read_only_fields = ('start_time', 'tutee', 'ambassador',
                            'day', 'course', 'session_canceled', 'display_link', 'zoom_pmi')


class WeeklyScheduleSerializer(ModelSerializer):
    course = serializers.ReadOnlyField(source='course.__str__')
    tutee = serializers.ReadOnlyField(source='tutee.get_full_name')
    ambassador = serializers.ReadOnlyField(source='ambassador.get_full_name')
    day = serializers.ReadOnlyField(source='get_day_display')
    start_time = serializers.ReadOnlyField(source='get_american_time')

    class Meta:
        model = TutoringSession
        fields = ('start_time', 'tutee', 'ambassador', 'day', 'course')
        read_only_fields = ('start_time', 'tutee', 'ambassador',
                            'day', 'course', 'session_canceled')


class SessionCreationSerializer(ModelSerializer):
    '''
    Serializer for creating a session manually
    '''

    class Meta:
        model = TutoringSession
        fields = ('start_time', 'tutee', 'ambassador',
                  'course', 'start_date', 'end_date')


class SessionSerializer(ModelSerializer):
    course = serializers.ReadOnlyField(source='course.__str__')
    tutee = serializers.ReadOnlyField(source='tutee.get_full_name')
    ambassador = serializers.ReadOnlyField(source='ambassador.get_full_name')
    day = serializers.ReadOnlyField(source='get_day_display')
    start_time = serializers.ReadOnlyField(source='get_american_time')

    class Meta:
        model = TutoringSession
        fields = ('start_time', 'tutee', 'ambassador', 'id', 'course', 'day')
