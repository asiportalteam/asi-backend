from datetime import timedelta, datetime
from asi_api.globals import MIN_DAYS_TO_SCHEDULE, SEMESTER_END_DATE
from custom_users.models import User
from courses.models import Course
from django.utils import timezone
from django.db import models

DAYS = (
    (1, 'Monday'),
    (2, 'Tuesday'),
    (3, 'Wednesday'),
    (4, 'Thursday'),
    (5, 'Friday'),
)

ROOMS = (
        ('101A', '101A'),
        ('101B', '101B'),
        ('101C', '101C'),
        ('101D', '101D'),
)


class TutoringSession(models.Model):
    ambassador = models.ForeignKey(User,
                                   related_name='ambassador_tutoring_sessions')
    course = models.ForeignKey(Course,
                               related_name='course_tutoring_sessions')
    tutee = models.ForeignKey(User,
                              related_name='tutee_tutoring_sessions')
    start_date = models.DateField()
    end_date = models.DateField()
    day = models.IntegerField(choices=DAYS, default=1)
    room = models.CharField(choices=ROOMS, default='101A', max_length=4)
    start_time = models.TimeField()
    session_canceled = models.BooleanField(default=False)

    @staticmethod
    def get_start_and_end_date(day_of_week, scheduled_date, end_date=SEMESTER_END_DATE):
        start_diff = day_of_week - scheduled_date.isoweekday()
        end_diff = end_date.isoweekday() - day_of_week
        if (scheduled_date.isoweekday() == 7 and start_diff == -6):
            actu_start_date = scheduled_date + timedelta(weeks=1, days=1)
        if start_diff >= MIN_DAYS_TO_SCHEDULE:
            actu_start_date = scheduled_date + timedelta(days=start_diff)
        else:
            actu_start_date = scheduled_date + \
                timedelta(weeks=1, days=start_diff)
        actu_end_date = end_date - timedelta(days=end_diff)
        return (actu_start_date, actu_end_date)

    @classmethod
    def create_from_request(cls, request):
        session = cls()
        session.day = request.availability.day
        session.start_time = request.availability.start_time
        session.ambassador = request.availability.ambassador

        session.course = request.course
        session.tutee = request.submitted_by
        session.room = request.room

        # figure out those times...
        session.start_date, session.end_date = cls.get_start_and_end_date(
            session.day, timezone.now().date())
        return session

    def get_end_time(self):
        '''
        this is so ugly lmao
        Why is it so hard to add 1 hour to time in python??
        '''
        return (datetime.combine(timezone.now().date(), self.start_time) + timedelta(hours=1)).time().strftime('%-I:%M %p')

    def get_american_time(self):
        '''
        'merica best time format
        F U C K 24 hour time
        '''
        return self.start_time.strftime('%I:%M %p')
    
    def get_dates(self):
        '''
        Returns a list of dates ranging from the start to the end of when tutoring sessions should occur.
        '''
        if self.end_date == self.start_date:
            dates = [self.start_date]
        else:
            dates = [(self.start_date + timedelta(days=x))
                    for x in range((self.end_date - self.start_date).days)]
            # very good
            dates = list(filter(lambda x: (x.isoweekday() == self.day), dates))
        return dates

    def __str__(self):
        return "{} with {} on {}@{} for {}{}".format(
            self.ambassador.get_full_name(),
            self.tutee.get_full_name(),
            self.get_day_display(),
            self.get_american_time(),
            self.course.subject,
            self.course.number)

    @property
    def general_display(self):
        return "{} with {}".format(
                self.course,
                self.tutee.get_full_name())

    @property
    def timesheet_display(self):
        return "{} on {} for {}{} with {}".format(
            self.get_american_time(),
            self.get_day_display(),
            self.course.subject,
            self.course.number,
            self.tutee.get_full_name()
        )
