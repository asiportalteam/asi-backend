from django.db import models

'''
An object which represents a Course that is offered for tutoring.

-subject : a 3 letter code which identifies what area this course belongs to e.g. COP == Computer Programming MAD == Discrete Mathematics
-number : The 4 letter number which combined with the subject identifies a specific course e.g. COP2210 == Programming 1
-full_name : the commonly known name of the course . e.g. COP2210 is known as Programming 1 and COT3100 is known as Discrete Structures
-professor : Necessary for specific courses. e.g. CDA3103 with Trevor Cickovski and CDA3103 with Norman Pestaina
-offered : Whether the course is currently being offered for ASI tutoring
'''


class Course(models.Model):
    subject = models.CharField(max_length=3)
    number = models.CharField(max_length=4)
    full_name = models.CharField(max_length=225)
    professor = models.CharField(max_length=45, blank=True)
    offered = models.BooleanField(default=True)

    '''
    Return a nice string representation of this course.
    '''

    def __str__(self):
        if self.professor:
            return '{}{} taught by {}'.format(
                self.subject, self.number, self.professor)
        else:
            return '{}{}'.format(self.subject, self.number)
