from .models import Course
from django.db.models import Count, Q
from custom_users.models import User
from .serializers import CourseSerializer
from availabilities.serializers import RequestAvailabilitySerializer
from availabilities.models import Availability
from rest_framework import generics, status
from tutoring_sessions.models import TutoringSession
from rest_framework.response import Response
from availabilities.models import DAYS


def num_to_day(num):
    return ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'][num - 1]


class CourseList(generics.GenericAPIView):
    '''
    CourseList
    Simple list view. Will just grab all courses.
    '''
    serializer_class = CourseSerializer

    def get(self, request):
        qs = Course.objects.all()
        content = self.serializer_class(qs, many=True)
        return Response(content.data, status=status.HTTP_200_OK)


class AvailabilityByCourse(generics.GenericAPIView):
    '''
    Returns all courses as well as the valid availability for those 
    courses.
    '''

    def get(self, request):
        valid = User.objects.filter(is_tutor=True)
        times = Availability.objects.filter(ambassador__in=valid,
                                            can_schedule=True,
                                            is_scheduled=False)
        over = [user for user in valid if user.is_overtime()]
        times = times.exclude(ambassador__in=over)

        # expirimental
        too_many_sessions = TutoringSession.objects.filter(session_canceled=False).values('start_time', 'day')\
            .annotate(sessions_at_time=Count('start_time')).order_by('-sessions_at_time').exclude(sessions_at_time__lt=7)\
            .values('start_time', 'day')
        q_list = [Q(start_time=x.get('start_time'), day=x.get('day'))
                  for x in too_many_sessions]
        if q_list:
            q_filter = q_list.pop()
            for new_q in q_list:
                q_filter |= new_q
            times = times.exclude(q_filter)

        courses = Course.objects.filter(offered=True).order_by('subject', 'number')

        content = []
        for course in courses:
            course_serialzed = CourseSerializer(course).data
            course_serialzed['times'] = dict()
            times_available = False
            for day in range(1, 6):
                times_for_course = times.filter(
                    ambassador__courses_can_tutor=course,
                    day=day).order_by('start_time')
                if(times_for_course.count() > 0):
                    times_available = True
                serialized = RequestAvailabilitySerializer(
                    times_for_course, many=True)
                course_serialzed['times'][num_to_day(day)] = serialized.data
            course_serialzed['times_available'] = times_available
            content += [course_serialzed]
        return Response(content, status=status.HTTP_200_OK)
