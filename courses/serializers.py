from .models import Course
from rest_framework import serializers

'''
Course Serializer
Serializes only the most basic fields
'''
class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('id', 'subject', 'number', 'full_name', 'professor')
