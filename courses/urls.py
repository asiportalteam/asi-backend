from django.conf.urls import url
from courses import views

urlpatterns = [
        url(r'^list/$', views.CourseList.as_view()),
        url(r'^all/available-times$', views.AvailabilityByCourse.as_view(), name='available_times'),
]
