from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APITestCase
from availabilities.models import Availability
from custom_users.models import User
from courses.models import Course


# Create your tests here.

class AvailabilityByCourseEndPointTest(APITestCase):

    def setUp(self):
        self.user = User.objects.create(
                email='user@fiu.edu',
                first_name='First',
                last_name = 'Name',
                )
        self.jwt_token= self.user.token
        self.course = Course.objects.create(
                subject = 'MAC',
                number = '2312',
                professor = 'Dr. Someone',
                )

    def test_that_when_no_tutors_no_times_returned(self):
        url = reverse('courses:available_times')
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.get(url)
        self.assertEqual(response.data[0].get('times'), [])
    
    def test_that_when_tutotrs_times_returned(self):
        amb = User.objects.create(
                email = 'amb@fiu.edu',
                username = 'amb@fiu.edu',
                first_name = 'Amb',
                last_name = 'Assador',
                is_tutor = True,
                )
        amb.courses_can_tutor.add(self.course)
        amb.save()
        avas = Availability.make_availabilities(amb)
        avas[0].can_schedule = True
        avas[0].save()
        url = reverse('courses:available_times')
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.get(url)
        self.assertNotEqual(response.data[0].get('times'), [])
        self.assertEqual(response.data[0].get('times')[0]['id'], avas[0].id)
