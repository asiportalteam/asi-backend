from django.contrib import admin
from .models import Course

# Register your models here.


class CourseAdmin(admin.ModelAdmin):
    fields = ('subject', 'number', 'professor', 'full_name', 'offered')
    list_display = ('subject', 'number', 'professor', 'offered')
    list_filter = ('offered', )


admin.site.register(Course, CourseAdmin)
