# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-10-01 14:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cancelation_requests', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cancelationrequest',
            name='status',
            field=models.CharField(choices=[('D', 'Pending'), ('B', 'Processed'), ('C', 'Withdrawn')], default='D', max_length=20),
        ),
    ]
