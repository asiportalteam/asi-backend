from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from courses.models import Course
from .models import CancelationRequest


class OfferedCourseOnlyFilter(admin.SimpleListFilter):
    title = _('course')
    parameter_name = 'course'

    def lookups(self, request, model_admin):
        courses = Course.objects.filter(offered=True)
        return (
            (course.id, _(course.subject + course.number)) for course in courses)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(tutoring_session__course=self.value())
        else:
            return queryset


# Register your models here.
class CancelationRequestAdmin(admin.ModelAdmin):
    list_display = ['course', 'who_submitted', 'submitted_by_email', 'tutor', 'reason', 'created_at', 'status']
    list_filter = ['status', OfferedCourseOnlyFilter, 'submitted_by']

    def who_submitted(self, obj):
        return obj.submitted_by.get_full_name()

    def tutor(self, obj):
        return obj.tutoring_session.ambassador.get_full_name()

    def submitted_by_email(self, obj):
        return obj.submitted_by.email

    def course(self, obj):
        return obj.tutoring_session.course


admin.site.register(CancelationRequest, CancelationRequestAdmin)
