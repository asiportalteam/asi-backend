from rest_framework.response import Response
from rest_framework import generics, status
from .models import CancelationRequest
from .serializers import CancellationRequestsSerializer, CreateCancellationRequestSerializer, UpdateCancellationRequestSerializer
from .utility import update_availability, update_surveys, update_tutoring_cancel_status
from emailer.mailer import cancellation_processed, cancellation_created
#, CreateCancellationRequestSerializer
class CancellationRequestListView(generics.GenericAPIView):

    def get(self, request):
        print(request.user)
        request = CancelationRequest.objects.all()
        serialized = CancellationRequestsSerializer(request, many = True)
        return Response(serialized.data, status=status.HTTP_200_OK)

class CancellationRequestCreateView(generics.GenericAPIView):

    serializer_class = CreateCancellationRequestSerializer

    def post(self, request):
        serialized = CreateCancellationRequestSerializer(data=request.data,
                context={
                    'request':request
                    })
        serialized.is_valid(raise_exception=True)
        crequest = serialized.save()
        update_tutoring_cancel_status(crequest)
        cancellation_processed(crequest)
        update_availability(crequest)
        update_surveys(crequest)
        crequest.status = 'B'
        crequest.save()
        return Response({'message': 'We processed a cancellation request!', 'id': crequest.id},
                status=status.HTTP_200_OK)

class UpdateCancellationRequestView(generics.GenericAPIView):

    serializer_class = UpdateCancellationRequestSerializer

    def post(self, request):
        cancellationModel = CancelationRequest.objects.get(id = request.data["id"])
        serialized = UpdateCancellationRequestSerializer( cancellationModel, data = request.data, partial = True)
        serialized.is_valid(raise_exception=True)
        serialized.update(cancellationModel,serialized.validated_data)


        return Response({'msg': 'We finalized a cancellation request!', 'id': request.data["id"]},
                status=status.HTTP_202_ACCEPTED)

