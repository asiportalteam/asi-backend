from django.apps import AppConfig


class CancelationRequestsConfig(AppConfig):
    name = 'cancelation_requests'
