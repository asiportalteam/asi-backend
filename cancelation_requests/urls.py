from django.conf.urls import url
from cancelation_requests import views

urlpatterns = [
    url(r'^list/?$', views.CancellationRequestListView.as_view()),
    url(r'^create/?$', views.CancellationRequestCreateView.as_view()),
    url(r'^update/?$', views.UpdateCancellationRequestView.as_view())
]
