from django.db import models
from tutoring_sessions.models import TutoringSession
from custom_users.models import User

STATUS_CODES = (
    ('D', 'Pending'),
    ('B', 'Processed'),
    ('C', 'Withdrawn')
)


'''
Potential Status'
    Pending
    Processed
    Withdrawn*
'''

class CancelationRequest(models.Model):
    submitted_by = models.ForeignKey(User, related_name='canceled_sessions')
    reason = models.CharField(max_length=240)
    tutoring_session = models.ForeignKey(TutoringSession)
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=STATUS_CODES, 
            default='D', max_length=20)

    def __str__(self):
        return f"{self.submitted_by.get_full_name()} cancelation request for {self.tutoring_session.course}@{self.tutoring_session.get_american_time()} on {self.tutoring_session.get_day_display()}"

    #Note to self, you need to find a way to change the created_at, from the long messy string to only the date that the
    #tutoring was requested to cancel.
    def created_at_normal(self):
        string  = self.created_at.__str__()
        year = string[0] + string[1] + string [2] + string [3]
        month = string[5] + string [6]
        day = string [8] + string[9]
        return month + "/" +  day + "/" + year

    def return_reason(self):
        string = self.reason

        return string
