from surveys.models import AmbassadorSurvey, TuteeSurvey

def update_availability(cancellation_model):
    ambassador = cancellation_model.tutoring_session.ambassador
    availability_time = cancellation_model.tutoring_session.start_time
    availability_day = cancellation_model.tutoring_session.day
    actual_availability = ambassador.availabilities.get(start_time=availability_time, day=availability_day)
    actual_availability.can_schedule = True
    actual_availability.is_scheduled = False
    actual_availability.save()

def update_surveys(cancellation_model):
    created_at = cancellation_model.created_at
    session = cancellation_model.tutoring_session
    ambassador_survey = AmbassadorSurvey.objects.filter(submitted=False, tutoring_session = session, session_date__gte = created_at)
    tutee_survey = TuteeSurvey.objects.filter(submitted=False, tutoring_session = session, session_date__gte = created_at)

    for every_survey in tutee_survey:
        every_survey.delete()

    for every_survey in ambassador_survey:
        every_survey.delete()

def update_tutoring_cancel_status(cancellation_model):
    cancellation_model.tutoring_session.session_canceled = not cancellation_model.tutoring_session.session_canceled
    cancellation_model.tutoring_session.save()
