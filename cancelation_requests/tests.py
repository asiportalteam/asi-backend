from custom_users.models import User
from tutoring_sessions.models import TutoringSession
from courses.models import Course
from rest_framework.test import APIRequestFactory, force_authenticate, APITestCase
from .views import CancellationRequestCreateView
from cancelation_requests.models import CancelationRequest
from django.utils import timezone
from datetime import timedelta
from .utility import update_availability, update_surveys
from availabilities.models import Availability
from surveys.models import TuteeSurvey, AmbassadorSurvey

class CreateCancelationRequestTest(APITestCase):
    def setUp(self):
        self.tutee = User.objects.create_user('John', 'Doe', 'jdoe001@fiu.edu', password='g00dp455',
                                              panther_id= "5214752", major = "Computer Science" )
        self.ambassador = User.objects.create_user('Jane', 'Doe', 'jdoe002@fiu.edu', password='g00dp455',
                                                   panther_id= "5214752", major = "Computer Science")
        self.c = Course.objects.create(subject='MAC', number='1140', full_name='calculus')
        self.tutoring_session = TutoringSession.objects.create(ambassador=self.ambassador,
                                                          tutee=self.tutee,
                                                               course=self.c,
                                                          start_date='2016-05-05',
                                                          end_date='2017-05-05',
                                                          start_time='12:00:00',
                                                          )
    def test_that_api_creates_request(self):
        reason = "I'm dropping"
        factory = APIRequestFactory()
        request = factory.post('/cancelation-requests/create',
                               data={'reason': reason,
                            'tutoring_session': self.tutoring_session.id},
                               format='json')
        force_authenticate(request, user=self.tutee)
        view = CancellationRequestCreateView.as_view()
        response = view(request)
        print(response.data)
        self.assertEqual(response.data['message'], "We got your request")
        self.assertEqual(response.data['id'], 1)


class UpdateCancellationRequestTest(APITestCase):

    def setUp(self):
        self.tutee = User.objects.create_user('John', 'Doe', 'jdoe001@fiu.edu', password='g00dp455',
                                              panther_id= "5214752", major = "Computer Science")
        self.ambassador = User.objects.create_user('Jane', 'Doe', 'jdoe002@fiu.edu', password='g00dp455',
                                                   panther_id="5214752", major="Computer Science")
        self.c = Course.objects.create(subject='MAC', number='1140', full_name='calculus')
        self.tutoring_session = TutoringSession.objects.create(ambassador=self.ambassador,
                                                               tutee=self.tutee,
                                                               course=self.c,
                                                               start_date= timezone.now() + timedelta(days = -2),
                                                               end_date=timezone.now() + timedelta(weeks = 4),
                                                               start_time='12:00:00',
                                                               )
        self.cancellation_request = CancelationRequest.objects.create(submitted_by = self.ambassador,
                                                                      reason = "I am cancelling",
                                                                      tutoring_session = self.tutoring_session,
                                                                      )
        self.availabity = Availability.objects.create(start_time = "12:00:00",
                                                      day = self.tutoring_session.start_date.isoweekday(),
                                                      is_scheduled = True,
                                                      can_schedule = False, ambassador = self.ambassador)

        self.tutee_surveys = TuteeSurvey.create_from_session(self.tutoring_session)
        self.ambassador_surveys = AmbassadorSurvey.create_from_session(self.tutoring_session)

    def test_that_api_updates_availabilty_request(self):
        update_availability(self.cancellation_request)
        actual_availability = Availability.objects.get(id = self.availabity.id)
        self.assertTrue(actual_availability.can_schedule)
        self.assertFalse(actual_availability.is_scheduled)


    def test_that_api_updates_surveys_request(self):
        update_surveys(self.cancellation_request)
        self.assertFalse(TuteeSurvey.objects.filter(tutoring_session = self.tutoring_session,
                                                    session_date__gt = self.cancellation_request.created_at))
        self.assertFalse(AmbassadorSurvey.objects.filter(tutoring_session=self.tutoring_session,
                                                    session_date__gt=self.cancellation_request.created_at))

