from rest_framework import serializers
from .models import CancelationRequest

class CancellationRequestsSerializer(serializers.ModelSerializer):


    day = serializers.ReadOnlyField(source = 'tutoring_session.get_day_display')

    created_at = serializers.ReadOnlyField(source = "created_at_normal")

    room = serializers.ReadOnlyField( source = "tutoring_session.room")

    time = serializers.ReadOnlyField( source = "tutoring_session.get_american_time")

    course = serializers.ReadOnlyField( source = "tutoring_session.course.__str__")

    ambassador = serializers.ReadOnlyField( source = "tutoring_session.ambassador.get_full_name")

    submitted_by = serializers.ReadOnlyField(
        source='submitted_by.get_full_name'
    )

    reason = serializers.ReadOnlyField( source = "return_reason")

    class Meta:
        model = CancelationRequest
        fields = ["submitted_by","status","created_at","reason", "room", "time", "course", "ambassador", "id", "day", "reason"]

class CreateCancellationRequestSerializer(serializers.ModelSerializer):

    submitted_by = serializers.PrimaryKeyRelatedField(
            read_only=True,
            default=serializers.CurrentUserDefault(),
            )
    class Meta:
        model = CancelationRequest
        fields = ['submitted_by', 'tutoring_session', 'reason']


class UpdateCancellationRequestSerializer(serializers.ModelSerializer):

    class Meta:
        model = CancelationRequest
        fields = ['status']

    def update(self, instance, validated_data):
        print(validated_data)
        instance.status = validated_data.get("status")
        instance.save()
        return instance