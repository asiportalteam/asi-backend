from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from itertools import chain
from collections import defaultdict
from rest_framework.response import Response
from rest_framework import generics, status

from cancelation_requests.models import CancelationRequest
from cancelation_requests.utility import update_availability, update_surveys
from timesheets.models import TutoringTimesheetEntry
from emailer.mailer import send_unexcused_absence, cancellation_processed
from .models import AmbassadorSurvey, TuteeSurvey
from .serializers import (
    AmbassadorSurveyListSerializer,
    AmbassadorSurveyDetailSerializer,
    AmbassadorSurveyUpdateSerializer,
    TuteeSurveyUpdateSerializer
)

from .utility import check_cancel

class LowScoreSurveysList(generics.GenericAPIView):

    def get(self, request):
        ts = TuteeSurvey.objects.filter(submitted=True)
        stats = {}
        for t in ts:
            if t.average <= 70 and t.average != -1:
                stats[str(t.pk)] = { 
                        'session' : t.tutoring_session.__str__(),
                                'comments' : t.comments,
                                'tutor' : t.tutoring_session.ambassador.get_full_name() }
        return Response( stats, status=status.HTTP_200_OK )

class IncompleteSurveysList(generics.GenericAPIView):

    def get(self, request):
        '''
        TODO: write something later
        '''

        ambassador_surveys = AmbassadorSurvey.objects.filter(submitted = False, session_date__lte=timezone.now())
        tutee_surveys = TuteeSurvey.objects.filter(submitted = False, session_date__lte=timezone.now())
        stats = dict()
        stats['missing_surveys'] = dict()

        for surv in ambassador_surveys:
            amb = surv.tutoring_session.ambassador.get_full_name()
            missing_tut = tutee_surveys.filter( tutoring_session=surv.tutoring_session).count()
            if amb not in stats['missing_surveys']:
                stats['missing_surveys'][amb] = defaultdict(lambda : 0 )
                stats['missing_surveys'][amb]['today'] = 0
                stats['missing_surveys'][amb]['past'] = 0
            if surv.session_date == timezone.now().date():
                stats['missing_surveys'][amb]['today'] += 1
            else:
                stats['missing_surveys'][amb]['past'] += 1
            stats['missing_surveys'][amb]['tutee_count'] += missing_tut
            stats['missing_surveys'][amb]['email'] = surv.tutoring_session.ambassador.email

        return Response(dict(stats), status=status.HTTP_200_OK)


class SurveysListView(generics.GenericAPIView):
    '''
    Endpoint which gathers both ambassador AND tutee surveys
    '''

    def get(self, request):
        '''
        Returns a list of both ambassador and tutee surveys for the user if they have any.
        This should really be done in a serializer but i like having a little more fine grain control
        of how the data is returned and I don't think you can accomplish this form of chaining with
        a serializer.
        '''
        user = request.user
        surveys = dict()
        ambs = AmbassadorSurvey.objects.filter(
            submitted=False,
            session_date__lte=timezone.now(),
            tutoring_session__ambassador=user).order_by('session_date')
        tuts = TuteeSurvey.objects.filter(
            submitted=False,
            session_date__lte=timezone.now(),
            tutoring_session__tutee=user).order_by('session_date')
        if ambs.count() == 0 and tuts.count() == 0:
            surveys['surveys'] = []
            surveys['msg'] = "You do not have any surveys!"
            return Response(surveys, status=status.HTTP_200_OK)
        else:
            if user.is_tutor:
                smash = sorted(chain(ambs, tuts),
                               key=lambda obj: obj.session_date)
                surveys['surveys'] = [
                    {
                        'id': obj.id,
                        'type': 'ambassador' if isinstance(
                            obj, AmbassadorSurvey) else 'tutee',
                        'session_date': obj.session_date,
                        'time': obj.tutoring_session.get_american_time(),
                        'ambassador': obj.tutoring_session.ambassador.get_full_name(),
                        'tutee': obj.tutoring_session.tutee.get_full_name(),
                        'course': "{}{}".format(
                            obj.tutoring_session.course.subject,
                            obj.tutoring_session.course.number),

                    }
                    for obj in smash
                ]
                surveys['msg'] = ""
                return Response(surveys, status=status.HTTP_200_OK)
            else:
                surveys['surveys'] = [
                    {
                        'id': obj.id,
                        'type': 'ambassador' if isinstance(
                            obj, AmbassadorSurvey) else 'tutee',
                        'session_date': obj.session_date,
                        'time': obj.tutoring_session.get_american_time(),
                        'ambassador': obj.tutoring_session.ambassador.get_full_name(),
                        'tutee': obj.tutoring_session.tutee.get_full_name(),
                        'course': "{}{}".format(
                            obj.tutoring_session.course.subject,
                            obj.tutoring_session.course.number),

                    }
                    for obj in tuts
                ]
                surveys['msg'] = ""
                return Response(surveys, status=status.HTTP_200_OK)


class AmbassadorSurveyListView(generics.GenericAPIView):
    '''
    Endpoint for ambasadors to gather their surveys
    '''

    def get(self, request):
        '''
        Executed when get method is called
        '''
        user = request.user
        incomplete = AmbassadorSurvey.objects.filter(
            submitted=False,
            session_date__gte=timezone.now(),
            tutoring_session__ambassador=user).order_by('session_date')
        if incomplete.count() == 0:
            return Response({'surveys': [], 'msg': ['No incomplete surveys!']}, status=status.HTTP_200_OK)
        else:
            serialized = AmbassadorSurveyListSerializer(incomplete, many=True)
            return Response({'msg': [], "surveys": serialized.data}, status=status.HTTP_200_OK)


class AmbassadorSurveyDetailView(generics.GenericAPIView):

    def get(self, request, pk):
        user = request.user
        surv = get_object_or_404(AmbassadorSurvey, pk=pk)
        if user != surv.tutoring_session.ambassador:
            return Response({'_errors': ["That survey doesn't belong to you!"]},
                            status=status.HTTP_401_UNAUTHORIZED)
        else:
            serialized = AmbassadorSurveyDetailSerializer(surv)
            return Response(serialized.data,
                            status=status.HTTP_200_OK)


class AmbassadorSurveyUpdateView(generics.GenericAPIView):
    serializer_class = AmbassadorSurveyUpdateSerializer

    def patch(self, request, pk):
        surv = AmbassadorSurvey.objects.get(pk=pk)
        response = dict()
        time_worked = 1
        serialzed = AmbassadorSurveyUpdateSerializer(surv, data=request.data)
        if request.user != surv.tutoring_session.ambassador:
            response['errors'] = "Thats not your survey!"
            response['heyNongMan'] = 'hey wait a minut...'
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        serialzed.is_valid(raise_exception=True)
        serialzed.save()
        # some notification > 6 hours
        if request.data.get('session_canceled') == True:
            toots = TuteeSurvey.objects.get(
                session_date=surv.session_date,
                tutoring_session=surv.tutoring_session)
            toots.comments = "Session canceled"
            time_worked = 0
            toots.submitted = True
            toots.save()
        # hour worked 
        if request.data.get('tutee_absent') == True:
            toots = TuteeSurvey.objects.get(
                session_date=surv.session_date,
                tutoring_session=surv.tutoring_session)
            # some notification given
            if request.data.get('was_excused'):
                toots.comments = "Excused absence"
            # no notification given
            else:
                toots.comments = "Unexcused absence"
                send_unexcused_absence(toots.tutoring_session,toots.session_date)
            toots.submitted = True
            toots.save()
        response['errors'] = None
        response['_debug'] = request.data
        response['msg'] = 'Successfully submitted survey!'
        # create timesheet entry
        entry = TutoringTimesheetEntry()
        entry.tutoring_session = surv.tutoring_session
        entry.ambassador = request.user
        entry.date = surv.session_date
        entry.time = time_worked
        entry.save()
        check_cancel(entry.tutoring_session)
        return Response(response, status=status.HTTP_200_OK)


class TuteeSurveyUpdateView(generics.GenericAPIView):
    serializer_class = TuteeSurveyUpdateSerializer

    def patch(self, request, pk):
        surv = TuteeSurvey.objects.get(pk=pk)
        response = dict()
        serialzed = TuteeSurveyUpdateSerializer(surv, data=request.data)
        if request.user != surv.tutoring_session.tutee:
            response['errors'] = "Thats not your survey!"
            response['more'] = request.user
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        serialzed.is_valid(raise_exception=True)
        serialzed.save()
        response['errors'] = None
        response['whyAreYouLookingAtThis'] = "Nosy."
        response['msg'] = 'Successfully submitted survey!'
        return Response(response, status=status.HTTP_200_OK)
