from rest_framework import serializers
from .models import AmbassadorSurvey, TuteeSurvey


class AmbassadorSurveyListSerializer(serializers.ModelSerializer):
    course = serializers.ReadOnlyField(
        source='tutoring_session.course.__str__')
    day = serializers.ReadOnlyField(source='tutoring_session.get_day_display')
    time = serializers.ReadOnlyField(
        source='tutoring_session.get_american_time')

    tutee = serializers.ReadOnlyField(
        source='tutoring_session.tutee.get_full_name')

    class Meta:
        model = AmbassadorSurvey
        fields = ('id', 'course', 'tutee', 'day', 'time', 'session_date')
        read_only_fields = ('id', 'course', 'tutee',
                            'day', 'time', 'session_date')


class AmbassadorSurveyUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AmbassadorSurvey
        fields = ('id', 'session_canceled', 'tutee_absent', 'was_excused', 'rating_1',
                  'rating_2', 'rating_3', 'rating_4', 'rating_5', 'comments')
        read_only_fields = ('id',)

    def update(self, instance, validated_data):
        instance.rating_1 = validated_data.get('rating_1')
        instance.rating_2 = validated_data.get('rating_2')
        instance.rating_3 = validated_data.get('rating_3')
        instance.rating_4 = validated_data.get('rating_4')
        instance.rating_5 = validated_data.get('rating_5')
        instance.comments = validated_data.get('comments')
        instance.tutee_absent = validated_data.get('tutee_absent')
        instance.was_excused = validated_data.get('was_excused')
        instance.session_canceled = validated_data.get('session_canceled')
        instance.submitted = True
        instance.save()
        return instance


class TuteeSurveyUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TuteeSurvey
        fields = ('id', 'rating_1', 'on_time', 'equipped', 'engaging',
                  'rating_2', 'rating_3', 'rating_4', 'rating_5', 'rating_6', 'rating_7', 'comments')
        read_only_fields = ('id',)

    def update(self, instance, validated_data):
        instance.rating_1 = validated_data.get('rating_1')
        instance.rating_2 = validated_data.get('rating_2')
        instance.rating_3 = validated_data.get('rating_3')
        instance.rating_4 = validated_data.get('rating_4')
        instance.rating_5 = validated_data.get('rating_5')
        instance.rating_6 = validated_data.get('rating_6')
        instance.rating_7 = validated_data.get('rating_7')
        instance.on_time = validated_data.get('on_time')
        instance.equipped = validated_data.get('equipped')
        instance.engaging = validated_data.get('engaging')
        instance.comments = validated_data.get('comments')
        instance.submitted = True
        instance.save()
        return instance


class AmbassadorSurveyDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = AmbassadorSurvey
        fields = ('id', 'rating_1', 'rating_2', 'rating_3', 'session_date')
        read_only_fields = ('id', 'rating_1', 'rating_2',
                            'rating_3', 'session_date')

    rating_1 = serializers.ReadOnlyField(source='rating_1_text')
    rating_2 = serializers.ReadOnlyField(source='rating_2_text')
    rating_3 = serializers.ReadOnlyField(source='rating_3_text')
