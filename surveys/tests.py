import datetime
from django.urls import reverse
from rest_framework.test import APITestCase
from courses.models import Course
from tutoring_sessions.models import TutoringSession
from custom_users.models import User
from django.test import TestCase
from .models import AmbassadorSurvey, TuteeSurvey

class CommonSetUp(TestCase):
    def setUp(self):
        self.amb = User.objects.create(
                email='ambassador@fiu.edu',
                username='ambassador@fiu.edu',
                first_name='amb',
                last_name='assador',
                is_tutor = True,
                )
        self.tut = User.objects.create(
                email='tutee@fiu.edu',
                username='tutee@fiu.edu',
                first_name='tut',
                last_name='ee',
                is_tutor = True,
                )
        self.course = Course.objects.create(
                subject = 'MAC',
                number = '2311',
                full_name = 'Calculus juan',
                )
        self.ts = TutoringSession.objects.create(
                course = self.course,
                tutee = self.tut,
                ambassador = self.amb,
                start_date = datetime.date(2017,7,3),
                end_date = datetime.date(2017,7,20),
                day = 1,
                start_time = datetime.time(11,0,0)
                )

class APICommonSetUp(CommonSetUp, APITestCase):
    def setUp(self):
        super(APICommonSetUp, self).setUp()
        self.survs = AmbassadorSurvey.create_from_session(self.ts)
        self.t_survs = TuteeSurvey.create_from_session(self.ts)
        self.jwt_token = self.amb.token

class AmbassadorSurveyModelTest(CommonSetUp):
    def test_that_surveys_get_created_for_each_date(self):
        expected_dates = self.ts.get_dates()
        survs = AmbassadorSurvey.create_from_session(self.ts)
        self.assertEqual(len(survs), len(expected_dates))
        for date,survey in zip(expected_dates, survs):
            self.assertEqual(date, survey.session_date)

class AmbassadorSurveyListViewTest(APICommonSetUp):

    def test_that_incomplete_surveys_are_shown(self):
        url = reverse('surveys:all_list')
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        survz = AmbassadorSurvey.objects.filter(tutoring_session=self.ts, submitted=False)
        self.assertEqual(len(response.data.get('surveys')), len(survz))
        for surv in survz:
            surv.submitted = True
            surv.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, len(response.data.get('surveys')))


class AmbassadorSurveyDetailViewTest(APICommonSetUp):

    def test_that_only_survs_for_an_amb_are_returned(self):
        jwt_tok_2 = self.tut.token
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(jwt_tok_2))
        url = reverse('surveys:amb_detail', kwargs={'pk' : self.survs[0].pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)

    def test_that_it_does_return_something(self):
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        url = reverse('surveys:amb_detail', kwargs={'pk' : self.survs[0].pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.data.get('_errors'))

class AmbassadorSurveyUpdateViewTest(APICommonSetUp):
    def test_that_can_complete_survey(self):
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        data = { 'rating_1' : 5,
                'rating_2' : 5,
                'rating_3' : 5,
                'tutee_absent': True,
                'was_excused': True,
                'session_canceled': False,
                'comments' : 'Test',
                }
        url = reverse('surveys:amb_update', kwargs={'pk' : self.survs[0].pk})
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, 200)
        surv = AmbassadorSurvey.objects.get(pk = self.survs[0].pk)
        self.assertTrue(surv.submitted)
        self.assertFalse(response.data.get('_errors'))

    def test_that_surveys_on_student_absent_get_marked_as_absent(self):
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        data = { 'rating_1' : 5,
                'rating_2' : 5,
                'rating_3' : 5,
                'comments' : 'Test',
                'was_excused': True,
                'tutee_absent': True,
                'session_canceled': False,
                }
        url = reverse('surveys:amb_update', kwargs={'pk' : self.survs[0].pk})
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, 200)
        surv = AmbassadorSurvey.objects.get(pk = self.survs[0].pk)
        self.assertTrue(surv.submitted)
        t_surv = TuteeSurvey.objects.get(
                tutoring_session=surv.tutoring_session,
                session_date=surv.session_date
                )
        self.assertEqual(t_surv.comments, "Excused absence")
        self.assertTrue(surv.tutee_absent)
        self.assertFalse(response.data.get('_errors'))

    def test_that_surveys_on_student_unexcused_are_unexcused(self):
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        data = { 'rating_1' : 5,
                'rating_2' : 5,
                'rating_3' : 5,
                'comments' : 'Test',
                'was_excused': False,
                'tutee_absent': True,
                'session_canceled': False,
                }
        url = reverse('surveys:amb_update', kwargs={'pk' : self.survs[0].pk})
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, 200)
        surv = AmbassadorSurvey.objects.get(pk = self.survs[0].pk)
        self.assertTrue(surv.submitted)
        t_surv = TuteeSurvey.objects.get(
                tutoring_session=surv.tutoring_session,
                session_date=surv.session_date
                )
        self.assertEqual(t_surv.comments, "Unexcused absence")
        self.assertTrue(surv.tutee_absent)
        self.assertFalse(response.data.get('_errors'))

    def test_that_canceled_surveys_are_marked(self):
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        data = { 'rating_1' : 5,
                'rating_2' : 5,
                'rating_3' : 5,
                'comments' : 'Test',
                'tutee_absent': False,
                'session_canceled': True,
                'was_excused': False,
                }
        url = reverse('surveys:amb_update', kwargs={'pk' : self.survs[0].pk})
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, 200)
        surv = AmbassadorSurvey.objects.get(pk = self.survs[0].pk)
        self.assertTrue(surv.submitted)
        self.assertTrue(surv.session_canceled)
        self.assertFalse(response.data.get('_errors'))
