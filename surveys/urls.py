from django.conf.urls import url
from surveys import views

urlpatterns = [
    url(r'^list/$', views.AmbassadorSurveyListView.as_view(), name='amb_list'),
    url(r'list/all/$', views.SurveysListView.as_view(), name='all_list'),
    url(r'^detail/(?P<pk>[0-9]+)$',
        views.AmbassadorSurveyDetailView.as_view(), name='amb_detail'),
    url(r'missing/$', views.IncompleteSurveysList.as_view(), name='missing'),
    url(r'^update/ambassador/(?P<pk>[0-9]+)$',
        views.AmbassadorSurveyUpdateView.as_view(), name='amb_update'),
    url(r'^detail/low-scores$', views.LowScoreSurveysList.as_view(), name='low_score'),
    url(r'^update/tutee/(?P<pk>[0-9]+)$',
        views.TuteeSurveyUpdateView.as_view(), name='tut_update'),
]
