from cancelation_requests.models import CancelationRequest
from cancelation_requests.utility import (update_tutoring_cancel_status, update_availability, update_surveys)
from emailer.mailer import cancellation_processed

def check_cancel(tutoring_session):
    surveys_unex_absent = tutoring_session.ambassadorsurvey.filter(tutee_absent = True, was_excused = False)

    survey_unex_absent_list = list(surveys_unex_absent.order_by("session_date"))
    print(surveys_unex_absent)

    last_two_sessions = survey_unex_absent_list[-2:]
    print(last_two_sessions)

    if len(last_two_sessions) == 2:
        date1 = last_two_sessions[0].session_date
        date2 = last_two_sessions[1].session_date
        print(date1, date2)

        time_delta = date2 - date1
        print(time_delta)

        if abs(time_delta.days) == 7:
            crequest = CancelationRequest.objects.create(submitted_by=tutoring_session.ambassador,
                                                         reason="Too many unexcused absences!",
                                                         tutoring_session=tutoring_session, status='B')
            crequest.tutoring_session.session_canceled = True
            crequest.tutoring_session.save()
            cancellation_processed(crequest)
            update_availability(crequest)
            update_surveys(crequest)
