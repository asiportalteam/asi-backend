from django.db import models
from tutoring_sessions.models import TutoringSession

'''
Thotzzz
 NO thotz at the mmt
'''


class Survey(models.Model):
    class Meta:
        abstract = True
    rating_1 = models.IntegerField(default=0)
    rating_2 = models.IntegerField(default=0)
    rating_3 = models.IntegerField(default=0)
    rating_4 = models.IntegerField(default=0)
    rating_5 = models.IntegerField(default=0)
    tutoring_session = models.ForeignKey(
        TutoringSession,
        related_name='%(class)s'
    )
    session_date = models.DateField()
    submitted_at = models.DateTimeField(auto_now=True)
    comments = models.CharField(max_length=255)
    submitted = models.BooleanField(default=False)

    @classmethod
    def create_from_session(cls, session):
        dates = session.get_dates()
        survs = [cls.objects.create(
            tutoring_session=session,
            session_date=date,
        ) for date in dates]
        return survs

    @classmethod
    def create_from_session_date(cls, session, date):
        surv = cls.objects.create(tutoring_session=session, session_date=date)
        return surv


class AmbassadorSurvey(Survey):
    session_canceled = models.BooleanField(default=False)
    tutee_absent = models.BooleanField(default=False)
    was_excused = models.BooleanField(default=False)

    def __str__(self):
        return "Ambassador Survey for {0} on {1}".format(
            self.tutoring_session,
            self.session_date
        )

    @property
    def rating_1_text(self):
        return "Student Has Made Great Progress During Session"

    @property
    def rating_2_text(self):
        return "Student Is Demonstrating Progress Over Time"

    @property
    def rating_3_text(self):
        return "Student Demonstrates A Quick Understanding Of The Material When Challenged (With A Practice Problem/Exercise)"

    @property
    def rating_4_text(self):
        return "Student Is Attentive And An Active Participant During The Session"

    @property
    def rating_5_text(self):
        return "Student Came Prepared For The Session"

class TuteeSurvey(Survey):
    wore_shirt = models.BooleanField(default=False)
    on_time = models.BooleanField(default=False)
    equipped = models.BooleanField(default=False)
    engaging = models.BooleanField(default=False)
    rating_6 = models.IntegerField(default=0)
    rating_7 = models.IntegerField(default=0)

    def __str__(self):
        return "Tutee Survey for {0} on {1}".format(
            self.tutoring_session,
            self.session_date
        )

    @property
    def rating_1_text(self):
        return "It is clear the ambassador knows and understands the subject matter of this course"

    @property
    def rating_2_text(self):
        return "The ambassador explains ideas and concepts clearly."

    @property
    def rating_3_text(self):
        return "The ambassador asks me questions and has me work sample problems."

    @property
    def rating_4_text(self):
        return "The ambassador listens to me and tries to understand my problems."

    @property
    def rating_5_text(self):
        return "The ambassador is friendly and courteous with me."

    @property
    def rating_6_text(self):
        return "The ambassador is trying to accommodate my learning style."

    @property
    def rating_7_text(self):
        return "The session is helpful and improved my understanding of the subject."

    @property
    def average(self):
        if self.comments == "Session canceled" or self.comments == "Student was not at session":
            return -1
        else:
            return (self.rating_1 + self.rating_2 + self.rating_3 + self.rating_4 + self.rating_5 + self.rating_6 + self.rating_7) / (7*5) * 100

