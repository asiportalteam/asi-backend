from django.contrib import admin
from django.contrib.admin import DateFieldListFilter
from django.utils.translation import gettext_lazy as _
from custom_users.models import User
from .models import AmbassadorSurvey, TuteeSurvey
from datetime import timedelta
from django.utils import timezone
from django.db import models


class AmbassadorOnlyFilter(admin.SimpleListFilter):
    title = _('Ambassadors')
    parameter_name = 'ambassador'

    def lookups(self, request, model_admin):
        qs = User.objects.filter(is_tutor=True)
        return (
            (user.id, _(user.get_full_name())) for user in qs)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(tutoring_session__ambassador=self.value())
        else:
            return queryset


class DateYesterdayFieldListFilter(DateFieldListFilter):
    def __init__(self, field, request, params, model, model_admin, field_path):
        super().__init__(field, request, params, model, model_admin, field_path)

        now = timezone.localtime()

        if isinstance(field, models.DateTimeField):
            today = now.replace(hour=0, minute=0, second=0, microsecond=0)
        else:       # field is a models.DateField
            today = now.date()

        yesterday = today - timedelta(days=1)

        links_start = self.links[:2]  # anyday and today
        link_yesterday = (_('Yesterday'), {
                self.lookup_kwarg_since: str(yesterday),
                self.lookup_kwarg_until: str(today),
            })

        self.links = (*links_start, link_yesterday, *self.links[2:])


class AmbassadorSurveyAdmin(admin.ModelAdmin):
    list_display = ('tutoring_session', 'session_date', 'submitted')
    list_filter = (('session_date', DateYesterdayFieldListFilter),
                   'session_canceled', 'tutee_absent', AmbassadorOnlyFilter)


class TuteeSurveyAdmin(admin.ModelAdmin):
    list_display = ('tutoring_session', 'session_date', 'submitted')
    list_filter = (('session_date', DateYesterdayFieldListFilter), 'submitted', AmbassadorOnlyFilter)


admin.site.register(AmbassadorSurvey, AmbassadorSurveyAdmin)
admin.site.register(TuteeSurvey, TuteeSurveyAdmin)
