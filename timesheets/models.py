from django.db import models
from custom_users.models import User
from tutoring_sessions.models import TutoringSession

# Create your models here.


class TutoringTimesheetEntry(models.Model):
    class Meta:
        unique_together = (('tutoring_session', 'date'),)
    tutoring_session = models.ForeignKey(TutoringSession)
    ambassador = models.ForeignKey(User)
    date = models.DateField()
    time = models.PositiveIntegerField()

    def __str__(self):
        return "{} worked for {} hour on {}".format(
            self.ambassador.get_full_name(),
            self.time,
            self.date
        )
