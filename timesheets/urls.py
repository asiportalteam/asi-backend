from django.conf.urls import url
from timesheets import views


urlpatterns = [
    url(r'^submit/$',
        views.SubmitTimesheetsView.as_view(), name='submit'),
    url(r'^download/all$', views.TimesheetDownloadView.as_view(), name='download_all'),
    url(r'^download/legacy$', views.download_old_school, name='test'),
    url(r'^create-single/$', views.CreateSingleTimesheetEntry.as_view(), name='create'),
    url(r'^fetch/all$', views.FetchAllTimesheets.as_view(), name='fetch_all'),
    url(r'^in-range/?$',
        views.FetchTimesheetsInRange.as_view(), name='fetch'),
    url(r'^update/$',
        views.UpdateTimesheetEntryView.as_view(), name='update'),
    url(r'^delete/(?P<id>[0-9]+)$', views.DeleteTimesheetEntryView.as_view(),
        name='delete'),
]
