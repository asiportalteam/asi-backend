import os
from django.views.decorators.csrf import csrf_exempt
from io import BytesIO
import zipfile
from asi_api.settings.base import BASE_DIR
from django.shortcuts import render
from django.core.files import File
from django.http import HttpResponse, FileResponse
from custom_users.models import User
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView
from .serializers import CreateTimesheetEntrySerializer, ViewTimesheetEntrySerializer, DownloadRequestSerializer
from .services import prepare_timesheets, zip_em_all
from .models import TutoringTimesheetEntry


class SubmitTimesheetsView(APIView):
    '''
    End point for ambassador to submit all there timesheets.
    '''
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CreateTimesheetEntrySerializer

    def get_serializer_context(self):
        return {
            'request': self.request,
        }

    def get_serializer(self, *args, **kwargs):
        kwargs['context'] = self.get_serializer_context()
        return self.serializer_class(*args, **kwargs)

    def post(self, request):
        '''
        Process their sheets.
        '''
        serialized = self.get_serializer(
            data=request.data, many=True)
        try:
            serialized.is_valid(raise_exception=True)
            serialized.save()
            return Response({'msg': 'Successfully created entries!'}, status=status.HTTP_200_OK)
        except ValidationError:
            return Response({'msg': "You have some dulpicate time sheet entries ;_; Please contact an administrator!"}, status=status.HTTP_400_BAD_REQUEST)


class FetchTimesheetsInRange(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        qs = TutoringTimesheetEntry.objects.filter(
            ambassador=request.user,
            date__range=[request.query_params.get(
                'start_date'), request.query_params.get('end_date')]
        ).order_by('date')
        serialized = ViewTimesheetEntrySerializer(qs, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)

class FetchAllTimesheets(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        qs = TutoringTimesheetEntry.objects.filter(ambassador=request.user).order_by('date')
        serialized = ViewTimesheetEntrySerializer(qs, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)


class UpdateTimesheetEntryView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def patch(self, request):
        try:
            entry = TutoringTimesheetEntry.objects.get(
                id=request.data.get('id'))
            if request.user != entry.ambassador:
                return Response({'msg': "That's not yours..."}, status=status.HTTP_400_BAD_REQUEST)
            if entry.time == 0:
                entry.time = 1
            else:
                entry.time = 0
            entry.save()
            qs = TutoringTimesheetEntry.objects.filter(
                ambassador=request.user).order_by('date')
            serialized = ViewTimesheetEntrySerializer(qs, many=True)
            return Response({'msg': 'Succesfully updated time!', 'sheets': serialized.data}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'msg': 'Something went wrong', 'err': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class DeleteTimesheetEntryView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request, id):
        try:
            entry = TutoringTimesheetEntry.objects.get(id=id)
            if request.user != entry.ambassdaor:
                return Response({'msg': "That's not yours..."}, status=status.HTTP_400_BAD_REQUEST)
            else:
                entry.delete()
                qs = TutoringTimesheetEntry.objects.filter(
                    ambassador=request.user
                ).order_by('date')
                serialized = ViewTimesheetEntrySerializer(qs, many=True)
                return Response({'msg': 'Succesfully deleted time!', 'sheets': serialized.data}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'msg': 'Something went wrong', 'err': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CreateSingleTimesheetEntry(APIView):
    def post(self, request):
        serialized = CreateTimesheetEntrySerializer(
            data=request.data, context={'request': request})
        serialized.is_valid(raise_exception=True)
        serialized.save()
        return Response({'msg': 'Successfully created entry!'}, status=status.HTTP_200_OK)

class TimesheetDownloadView(GenericAPIView):
    serializer_class = DownloadRequestSerializer

    def post(self, request):
        if request.user.is_superuser or request.user.email == 'mherlle@fiu.edu':
            tutors = User.objects.filter(is_tutor=True).exclude(email='admin@fiu.edu')
            f_names = []
            serialized = self.serializer_class(data=request.data)
            serialized.is_valid(raise_exception=True)
            for tutor in tutors:
                prepare_timesheets(
                        tutor,
                        request.data['start_date'],
                        request.data['end_date'])
            fname = zip_em_all()
            zip_file = open(fname, 'rb')
            resp = FileResponse(zip_file, content_type='application/zip')
            resp['Content-Disposition'] = 'attachment; filename=test.zip'
            return resp
        else:
            return HttpResponse("go fuck urself")

@csrf_exempt
def download_old_school(request):
    if request.user.is_superuser or request.user.email == 'mherlle@fiu.edu':
        tutors = User.objects.filter(is_tutor=True).exclude(email='admin@fiu.edu')
        f_names = []
        for tutor in tutors:
            prepare_timesheets(
                    tutor,
                    request.data['start_date'], 
                    request.data['end_date'],
                    )
            f_names.append('{}.xlsx'.format(tutor.get_full_name()))
        buff = BytesIO()
        curr_dirr = os.path.abspath(os.path.dirname(__file__))
        generated_dir = os.path.join(curr_dirr, 'generated')
        with zipfile.ZipFile(buff, 'w', zipfile.ZIP_DEFLATED) as zf:
            for name in f_names:
                full_path = os.path.join(generated_dir, name)
                print(full_path)
                zf.write(full_path, arcname=name)
            print(zf.infolist())
        resp = HttpResponse(content_type='application/zip')
        resp['Content-Disposition'] = 'attachment; filename=test.zip'
        resp.write(buff.getvalue())
        return resp
