from rest_framework.response import Response
from rest_framework import serializers
from .models import TutoringTimesheetEntry
from surveys.models import TuteeSurvey


class CreateTimesheetEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = TutoringTimesheetEntry
        fields = ('tutoring_session', 'date', 'time')

    def create(self, validated_data):
        validated_data['ambassador'] = self.context['request'].user
        return super(CreateTimesheetEntrySerializer, self).create(validated_data)


class ViewTimesheetEntrySerializer(serializers.ModelSerializer):
    tutoring_session = serializers.ReadOnlyField(
        source='tutoring_session.general_display')
    session_time = serializers.ReadOnlyField(
            source='tutoring_session.get_american_time')
    tutee_survey_status = serializers.SerializerMethodField()

    def get_tutee_survey_status(self, obj):
        ses = obj.tutoring_session
        message = ""
        try:
            tutee_survey = TuteeSurvey.objects.get(session_date=obj.date, tutoring_session=ses)
            message = "Yes" if tutee_survey.submitted else "No"
            return message
        except TuteeSurvey.DoesNotExist:
            return "Session cancelled"

    class Meta:
        model = TutoringTimesheetEntry
        fields = ('id', 'tutoring_session', 'date', 'session_time', 'time', 'tutee_survey_status')
        read_only_fields = ('id', 'tutoring_session', 'date', 'session_time', 'time')



class UpdateTimesheetEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = TutoringTimesheetEntry
        fields = ('id')
        read_only_fields = ('id')

class DownloadRequestSerializer(serializers.Serializer):
    start_date = serializers.DateField()
    end_date = serializers.DateField()
