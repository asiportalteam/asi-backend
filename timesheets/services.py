import os
import shutil
from openpyxl import Workbook, load_workbook
from openpyxl.drawing.image import Image
from django.utils import timezone
from timesheets.models import TutoringTimesheetEntry

today = timezone.now().date().strftime('%d_%b')
curr_dir = os.path.abspath(os.path.dirname(__file__))
template_dir = os.path.join(curr_dir, 'assets/timesheet_template.xlsx')
generated_dir = os.path.join(curr_dir, 'generated/')
img_dir = os.path.join(curr_dir, 'assets/asiclear.png')

def prepare_timesheets(tutor, start_date, end_date):
    wb = load_workbook(template_dir)
    sheets = TutoringTimesheetEntry.objects.filter(
        ambassador=tutor,
        date__range=[start_date, end_date]
        ).order_by('date', 'tutoring_session__start_time')
    work_sheet = wb.active
    work_sheet.title = '{} - {}'.format(start_date, end_date)
    if sheets:
        work_sheet.cell(1, 1, "Timesheet For {}".format(
            tutor.get_full_name().title()))
        entry_start_row = 3
        for sheet in sheets:
            work_sheet.cell(entry_start_row, 1, sheet.date)
            work_sheet.cell(entry_start_row, 2, sheet.tutoring_session.get_american_time())
            work_sheet.cell(entry_start_row, 3, sheet.tutoring_session.get_end_time()) 
            work_sheet.cell(entry_start_row, 4, "{}".format(sheet.tutoring_session.course)) 
            work_sheet.cell(entry_start_row, 5, sheet.tutoring_session.tutee.get_full_name().title())
            work_sheet.cell(entry_start_row, 6,sheet.tutoring_session.room)
            work_sheet.cell(entry_start_row, 7,sheet.time)
            entry_start_row += 1
    img = Image(img_dir)
    work_sheet.add_image(img, 'E9')
    output_dir = os.path.join(curr_dir, 'generated/{}.xlsx'.format(tutor.get_full_name()))
    wb.save(output_dir)

def zip_em_all():
    shutil.make_archive('{}'.format(today),'zip',generated_dir)
    return '{}.zip'.format(today)
