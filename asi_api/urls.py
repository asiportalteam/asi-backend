"""asi_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from django.conf.urls import url, include
from rest_framework_swagger.views import get_swagger_view
from django.contrib import admin

schema_view = get_swagger_view(title='ASI Portal API')

urlpatterns = [
    url(r'^$', schema_view),
    url(r'^api/admin/', admin.site.urls),
    url(r'^api/auth/token/get/', obtain_jwt_token, name='get_jwt_token'),
    url(r'^api/auth/token/refresh/', refresh_jwt_token),
    url(r'^api/accounts/', include('custom_users.urls', namespace='custom_user')),
    url(r'^api/availabilities/',
        include('availabilities.urls', namespace='availabilities')),
    url(r'^api/courses/', include('courses.urls', namespace='courses')),
    url(r'^api/tutoring-sessions/',
        include('tutoring_sessions.urls', namespace='tutoring_sessions')),
    url(r'^api/surveys/', include('surveys.urls', namespace='surveys')),
    url(r'^api/tutoring-requests/',
        include('tutoring_requests.urls', namespace='tutoring_requests')),
    url(r'^api/timesheets/', include('timesheets.urls', namespace='timesheets')),
    url(r'^api/cancellation-requests/', include('cancelation_requests.urls', namespace='cancellation_requests')),
    url(r'^api/announcements/', include('announcements.urls', namespace='announcements')),
]
