import datetime
EARLIEST_TIME = 11
LATEST_TIME = 22


# TODO Change this in production please.
SEMESTER_END_DATE = datetime.date(2022, 4, 22)

# Tutoring session booking stuff

# The minimum timeframe between a session being scheduled and the first
# session occuring is going to be 48 hours or 2 days
MIN_DAYS_TO_SCHEDULE = 1

# Number of workdays.. kinda dumb current mon-fri so 5 days
NUM_DAYS = 5
