from django.test import TestCase
from .models import User

# Create your tests here.

class CustomUserTestCase(TestCase):

    def setUp(self):
        self.email = 'jake@fiu.edu'
        self.first_name = 'Jake'
        self.last_name = 'Lopez'
        self.password = 'pythonwizkid69'
        self.u = User.objects.create_user(
                first_name=self.first_name,
                last_name=self.first_name,
                email=self.email,
                password=self.password,
                )

    def test_that_users_can_not_be_active(self):
        self.assertTrue(User.objects.all())
        user = User.objects.get(username=self.email)
        self.assertEqual(user.username, self.email)
        self.assertEqual(False, user.is_active)

    def test_that_accounts_can_be_activated(self):
        user = User.objects.get()
        activation_code = user.activation_code
        self.assertNotEqual("", activation_code)
        self.assertTrue(user.activate(activation_code))
        user = User.objects.get()
        self.assertTrue(user.is_active)
