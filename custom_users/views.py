from django.contrib.auth import authenticate
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from .models import User
from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from .serializers import (
    AccountCreationSerializer,
    LoginAccountSerializer,
    AccountListSerializer,
    PasswordResetSerializer,
    SubmitPasswordResetSerializer,
    PermissionSerializer, )
from .tokens import account_activation_token
from emailer.mailer import send_account_confirmation_code, send_password_reset_code
from .helpers import reset_password

'''
List all accounts in the system
returns everything read only
'''


class AccountListView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = AccountListSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('first_name', 'last_name', 'email',)


class TutorAccountListView(generics.ListAPIView):
    queryset = User.objects.filter(is_tutor=True)
    serializer_class = AccountListSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('first_name', 'last_name', 'email',)


class UpdatePantherIdView(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = AccountListSerializer

    def post(self, request):
        email = request.data.get('email', {})
        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            user.panther_id = request.data.get('panther_id', {})
            user.save()
            return Response({'jwt_token': user.token}, status=status.HTTP_200_OK)


class AccountCreationView(generics.GenericAPIView):
    '''
    View to create a new account.

    Returns the JWT token for the given user.
    '''
    permission_classes = (AllowAny,)
    serializer_class = AccountCreationSerializer

    def post(self, request):
        user = request.data.get('user', {})
        user['email'] = request.data.get('email', {})
        user['password'] = request.data.get('password', {})
        user['last_name'] = request.data.get('last_name', {})
        user['first_name'] = request.data.get('first_name', {})
        user['panther_id'] = request.data.get('panther_id', "")
        user['major'] = request.data.get('major', {})
        user['hear'] = request.data.get('hear', {})
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        # should send email at this point
        send_account_confirmation_code(user)
        content = {
            'token': user.token,
        }
        return Response(content, status=status.HTTP_201_CREATED)


'''
View to activate an account with a given account token

returns a JWT token if account activation is successful
'''


class ActivateAccountView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            user = User.objects.get(email=request.data.get('email'))
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None:
            code = request.data.get('code')
            activated = user.activate(code)
            if activated:
                token = user.token
                content = {
                    'activated': True,
                    'jwt_token': token,
                }
                return Response(content, status=status.HTTP_200_OK)
            else:
                content = {
                    'activated': False,
                    'jwt_token': None,
                    'errors': {'global': 'Wrong code'}
                }
                return Response(content, status=status.HTTP_400_BAD_REQUEST)
        else:
            content = {
                'activated': False,
                'jwt_token': None,
                'errors': {'global': 'No user with that account is registered'},
            }
            return Response(content, status=status.HTTP_400_BAD_REQUEST)


class LoginAccountView(generics.GenericAPIView):
    '''
    End point which allows logging in a user
    Returns a JWT token
    '''
    permission_classes = (AllowAny,)
    serializer_class = LoginAccountSerializer

    def post(self, request):
        content = dict()
        user = {}
        user['email'] = request.data.get('email')
        user['password'] = request.data.get('password')
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        u = authenticate(email=user['email'], password=user['password'])
        content['jwt_token'] = None
        content['errors'] = dict()
        def_status = status.HTTP_200_OK
        if u is not None and u.is_active:
            content['jwt_token'] = u.token
            content['email'] = u.email
            content['full_name'] = u.get_full_name()
            if u.panther_id == '':
                content['has_panther_id'] = False
            else:
                content['has_panther_id'] = True
        else:
            if not User.objects.filter(email=user['email']).exists():
                content['errors']['global'] = "There aren't any accounts registered to the email {}.".format(
                    user['email'])
                def_status = status.HTTP_400_BAD_REQUEST
            else:
                u = User.objects.get(email=user['email'])
                if not u.is_active:
                    content['errors']['global'] = 'This account has not been activated.'
                    def_status = status.HTTP_400_BAD_REQUEST
                if not u.check_password(user['password']):
                    content['errors']['global'] = "You've entered an incorrect password for this account"
                    def_status = status.HTTP_400_BAD_REQUEST
        return Response(content, status=def_status)


class GetUserPermissionView(generics.GenericAPIView):
    '''
    Endpoint for the sole purpose of retrieving a users permissions
    '''

    def get(self, request, pk):
        try:
            u = User.objects.get(pk=pk)
            content = PermissionSerializer(u)
            return Response(content.data, status=status.HTTP_200_OK)
        except ObjectDoesNotExist as e:
            content = {'errors': ['No such user.'],
                       'exception': e.__str__()}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)


class FetchCurrentUserView(generics.GenericAPIView):

    def get(self, request):
        return Response({
            'email': request.user.email,
            'id': request.user.id,
            'full_name': request.user.get_full_name(),
            'is_tutor': request.user.is_tutor,
            'is_active': request.user.is_active,
            'jwt_token': request.user.token,
            'permissions': request.user.permissions,
            'has_panther_id': request.user.panther_id != ''
        },
            status=status.HTTP_200_OK)


class RequestPasswordResetView(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PasswordResetSerializer

    def post(self, request):
        serialized = PasswordResetSerializer(data=request.data)
        serialized.is_valid(raise_exception=True)
        status = reset_password(serialized.data.get('email'))
        return status


class SendPasswordResetView(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = SubmitPasswordResetSerializer

    def post(self, request):
        serialized = SubmitPasswordResetSerializer(data=request.data)
        serialized.is_valid(raise_exception=True)
        passwd = serialized['password']
        try:
            user = User.objects.get(email=serialized.data.get('email'))
            if serialized.data.get('activation_code') == user.activation_code:
                user.set_password(serialized.data.get('password'))
                user.save()
                return Response({
                    'msg': "Password successfully changed.",
                    'jwt_token': user.token,
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    'msg': "Invalid activation code. "
                           "Please check your email for the correct code or request a new one."},
                    status=status.HTTP_400_BAD_REQUEST)
        except Exception:
            return Response({
                'msg': "We couldn't find an account with that email, please try signing up."},
                status=status.HTTP_400_BAD_REQUEST)
