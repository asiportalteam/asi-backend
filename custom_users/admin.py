from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import User


class UserAdmin(BaseUserAdmin):
    list_filter = ('is_tutor',)
    list_display = ('username', 'first_name', 'last_name', 'is_tutor')

    ''' 
    Have to over ride this method so we can edit the limits we set in our custom user model
    '''

    def get_fieldsets(self, request, obj=None):
        fs = super(BaseUserAdmin, self).get_fieldsets(request, obj)
        fs += (
            ('Limits',
             {
                 'fields':
                     ('session_limit', 'request_limit', 'sessions_per_course')
             },
             ),
            ('Course Administration',
             {
                 'fields':
                     ('courses_can_tutor', 'courses_lead', 'zoom_pmi'),
             },
             ),
            ('Activation Code',
             {
                 'fields':
                     ('activation_code', 'hear'),
             },
             ),
            ('Employee Status',
             {
                 'fields':
                     ('is_tutor', 'is_team_leader'),
             }
             ),
        )
        return fs


# Register your models here.
admin.site.register(User, UserAdmin)
