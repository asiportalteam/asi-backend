import random
from rest_framework_jwt.settings import api_settings
from django.db import models
from courses.models import Course
from datetime import timedelta
from django.utils import timezone
from django.contrib.auth.models import AbstractUser, BaseUserManager

CODE_LEN = 6
MAJORS = (('CS', 'Computer Science'),
          ('IT', 'Information Technology'),
          ('O', 'Other')
          )
HEAR = (('EMA', 'Email'),
        ('SOC', 'Social Media'),
        ('ORG', 'Student Organization'),
        ('ADV', 'Advisor'),
        ('PROF', 'Professor'),
        ('FRI', 'Friend'),
        ('O', 'Other'),
        ('U', 'Unknown')
        )


class UserManager(BaseUserManager):

    def create_user(self, first_name, last_name, email, panther_id, major, hear, password=None):
        # prepare the user model
        user = self.model(
            username=self.normalize_email(email),
            email=self.normalize_email(email),
            first_name=first_name,
            panther_id=panther_id,
            major=major,
            hear=hear,
            last_name=last_name)
        user.set_password(password)

        # create auth code
        random.seed()
        activation_code = str()
        for x in range(CODE_LEN):
            activation_code += str(random.randint(0, 9))
        user.activation_code = activation_code

        # make them inactive
        user.is_active = False
        user.save()
        return user

    def create_superuser(self, username, email, password=None, first_name='Jake', last_name='Lopez', ):

        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(first_name, last_name, email, password)
        user.is_superuser = True
        user.is_staff = True
        user.is_active = True
        user.save()
        return user


'''
Our base User model. Includes all the good stuff from Django + some extra fields for our personal use.

- session_limit : The maximum # of hours this User can work ( only applies to ambassadors )
- request_limit : The maximum # of tutoring requests and sessions a User can make. 
- courses_can_tutor : A grouping of Course objects which a given ambassador can tutor (only applies to ambassadors)
- courses_lead : A grouping of Course objects which a given ambassador can be the team leader of. ( Only applies to select ambassadors )
- sessions_per_course : The maximum number of sessions that a user can request per course
'''


class User(AbstractUser):
    email = models.EmailField(db_index=True, unique=True)
    activation_code = models.CharField(max_length=CODE_LEN, blank=True)
    session_limit = models.IntegerField(default=10)
    request_limit = models.IntegerField(default=3)
    sessions_per_course = models.IntegerField(default=2)
    courses_can_tutor = models.ManyToManyField(Course,
                                               related_name='tutors',
                                               blank=True)
    courses_lead = models.ManyToManyField(Course,
                                          related_name='leaders',
                                          blank=True)
    first_name = models.CharField(max_length=15)
    last_name = models.CharField(max_length=25)
    is_tutor = models.BooleanField(default=False)
    is_team_leader = models.BooleanField(default=False)
    is_compliance_officer = models.BooleanField(default=False)
    is_logistic_director = models.BooleanField(default=False)
    major = models.CharField(max_length=122, choices=MAJORS, default='CS')
    hear = models.CharField(max_length=122, choices=HEAR, default='U')
    panther_id = models.CharField(max_length=7, blank=True)
    zoom_pmi = models.CharField(max_length=32, default='[No PMI found]')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']
    objects = UserManager()

    def activate(self, code):
        if self.activation_code == code:
            self.is_active = True
            self.save()
            return self.is_active
        else:
            return False

    @property
    def permissions(self):
        if self.email == 'mherlle@fiu.edu':
            return 'myrian'
        elif self.is_logistic_director and self.is_team_leader or self.is_superuser:
            return 'everything'
        elif self.is_team_leader:
            return 'teamLeader'
        elif self.is_logistic_director:
            return 'logisticDirector'
        elif self.is_compliance_officer:
            return 'compliance'
        elif self.is_tutor:
            return 'ambassador'
        else:
            return 'student'

    @property
    def token(self):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(self)
        payload['permissions'] = self.permissions
        payload['isActive'] = self.is_active
        payload['fullName'] = self.get_full_name()
        token = jwt_encode_handler(payload)
        return token

    def is_overtime(self):
        schedulded = self.availabilities.filter(
            ambassador=self,
            is_scheduled=True, )
        if schedulded.count() >= self.session_limit:
            return True
        else:
            return False

    def gen_new_code(self):
        random.seed()
        activation_code = str()
        for x in range(CODE_LEN):
            activation_code += str(random.randint(0, 9))
        self.activation_code = activation_code
        self.save()


class ResetCode(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    expiration_date = models.DateTimeField()
    user = models.ForeignKey(User)

    def expiration_date(self):
        days_to_expire = timedelta(days=3)
        return self.creation_date - days_to_expire

    def check_valid(self):
        now = timezone.now()
        return self.expiration_date > now
