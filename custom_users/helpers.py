from .models import ResetCode, User
from rest_framework.response import Response
from rest_framework import status
from emailer.mailer import send_password_reset_code

SUCCESS = "SUCCESS"
NO_USER = "NO_USER"
REQUEST_EXISTS = "REQUEST_EXISTS"


CODES = {SUCCESS: Response({'msg': "Email with activation code was sent."},
                               status=status.HTTP_200_OK),
                               NO_USER: Response({'msg': "No user exists with that acocount."}, status=status.HTTP_400_BAD_REQUEST),
                               REQUEST_EXISTS: Response({'msg' : "You have already requested a reset code, please check your email."}, status=status.HTTP_400_BAD_REQUEST)
          }

def reset_password(email):
    status = check_user_valid(email)
    if status is not NO_USER:
        status = check_request_valid(email)
    return CODES.get(status)

def check_request_valid(email):
    if ResetCode.objects.filter(user__email=email).exists():
        return REQUEST_EXISTS
    else:
        user = User.objects.get(email=email)
        user.gen_new_code()
        ResetCode.objects.create(user=user)
        send_password_reset_code(user)
        return SUCCESS

def check_user_valid(email):
    if User.objects.filter(email=email).exists():
        return SUCCESS
    else:
        return NO_USER

