from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from .models import User


def subdomain_validator(email):
    subdomain = email.split('@')[1]
    if subdomain != 'fiu.edu':
        raise serializers.ValidationError("Only FIU emails are allowed!")


class AccountCreationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'password', 'first_name', 'last_name', 'panther_id', 'major', 'hear']

    email = serializers.EmailField(validators=[subdomain_validator, UniqueValidator(
        queryset=User.objects.all(),
        message="There is already an account registered with this email.")])
    password = serializers.CharField(
        min_length=8,
        max_length=128,
        write_only=True,
    )

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class LoginAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'password']
        read_only_fields = ('email', 'password')
    email = serializers.EmailField()
    password = serializers.CharField()


class PasswordResetSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['email']
        read_only_fields = ('email',)
    email = serializers.EmailField()

class SubmitPasswordResetSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'activation_code', 'password']
        read_only_fields = ['email', 'activation_code']

    email = serializers.EmailField()
    activation_code = serializers.CharField(min_length=6, max_length=6)
    password = serializers.CharField(
        min_length=8,
        max_length=128
    )


class AccountListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'pk']
        read_only_fields = ('email', 'first_name', 'last_name', 'pk')


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'pk', 'is_tutor',
                  'is_team_leader', 'is_logistic_director']
        read_only_fields = ('email', 'pk', 'is_tutor', 'is_team_leader',
                            'is_logistic_director')
