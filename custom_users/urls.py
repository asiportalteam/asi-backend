from django.conf.urls import url
from custom_users import views

urlpatterns = [
    url(r'^create/?$', views.AccountCreationView.as_view()),
    url(r'^login/?$', views.LoginAccountView.as_view()),
    url(r'^activate/$', views.ActivateAccountView.as_view()),
    url('^(?P<pk>[0-9]+)/get/permissions/$',
        views.GetUserPermissionView.as_view()),
    url(r'^list/', views.AccountListView.as_view()),
    url(r'^tutor-list/', views.TutorAccountListView.as_view()),
    url(r'^fetch/current$', views.FetchCurrentUserView.as_view()),
    url(r'^password-reset/request$', views.RequestPasswordResetView.as_view()),
    url(r'^password-reset/send$', views.SendPasswordResetView.as_view()),
    url(r'^request-password-reset/$', views.RequestPasswordResetView.as_view()),
    url(r'^update/panther-id/$', views.UpdatePantherIdView.as_view())
]
