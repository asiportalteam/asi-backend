from rest_framework import permissions


class IsTeamLeader(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_team_leader or request.user.is_superuser


class IsLogisticDirector(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_logistic_director or request.user.is_superuser
