from invoke import Responder
from fabric import Connection


with Connection("dev@asi.training", connect_kwargs = {'password' : 'realg00d' }) as c:
    sudopass = Responder(
            pattern=r'\[sudo\] password for dev:',
            response='realg00d\n',
            )

    remotepass = Responder(
            pattern=r"asiadmin@asiportal\.cis\.fiu\.edu's password:",
            response='451admin111\n')
    c.run('sudo scp asiadmin@asiportal.cis.fiu.edu:~/LATEST.tar.bz2 .', pty=True, watchers=[sudopass, remotepass])
    c.run('rm *.sql') # make sure there's no duplicates
    c.run('tar -xvf LATEST.tar.bz2')
    c.run('rm LATEST.tar.bz2')
    c.run('ls')
    c.run('dropdb asiportal')
    c.run('createdb asiportal')
    c.run('psql asiportal < $(ls *.sql)')
    c.run('rm *.sql') # clean up

    print("All done")
