from openpyxl import Workbook
from datetime import timedelta
from django.db.models import Count
from django.utils import timezone
from courses.models import Course
from surveys.models import AmbassadorSurvey, TuteeSurvey
from custom_users.models import User
from tutoring_requests.models import TutoringRequest
from tutoring_sessions.models import TutoringSession
from availabilities.models import Availability
from .totals import Totals

ALL_USERS = User.objects.all().exclude(username='admin@fiu.edu')
ALL_SESSIONS = TutoringSession.objects.all()
ALL_REQUESTS = TutoringRequest.objects.all()
TODAY=timezone.now()
SEVEN_DAYS_BEFORE=TODAY + timedelta(days=-7)
FNAME = '{}-report.xlsx'.format(TODAY.date())
wb = Workbook()

def insert_row(sheet, generator, col, row_offset):
    row_num = row_offset
    for row in generator():
        i = 0
        for vals in row:
            sheet.cell(column=col+i, row=row_num, value=vals)
            i += 1
        row_num += 1


def populate_book():
    totals_sheet = wb.active
    totals_sheet.title = 'Totals'
    totals_mod = Totals(TODAY)
    totals_sheet.cell(column=1, row=1, value="User Totals")
    insert_row(totals_sheet, totals_mod.get_user_stats, 1, 2)

    totals_sheet.cell(column=1, row=5, value="Availability Totals")
    insert_row(totals_sheet, totals_mod.get_availability_numbers, 1, 6)

    totals_sheet.cell(column=4, row=1, value="Request Totals")
    insert_row(totals_sheet, totals_mod.get_total_requests, 4, 2)

    totals_sheet.cell(column=4, row=6, value="Session Totals")
    insert_row(totals_sheet, totals_mod.get_session_totals, 4, 7)

    totals_sheet.cell(column=1, row=10, value="Ambassador Survey Totals")
    insert_row(totals_sheet, totals_mod.get_ambassador_survey_totals , 1, 11)

    totals_sheet.cell(column=4, row=10, value="Tutee Survey Totals")
    insert_row(totals_sheet, totals_mod.get_tutee_survey_totals , 4, 11)

    courses_sheet = wb.create_sheet(title='Courses')
    courses_sheet.cell(column=1, row=1, value="Tutors Per Course")
    insert_row(courses_sheet, get_tutors_per_course, 1, 2)

    courses_sheet.cell(column=4, row=1, value="Sessions Per Course")
    insert_row(courses_sheet, get_sessions_per_course, 4, 2)

    courses_sheet.cell(column=7, row=1, value="Requests Per Course")
    insert_row(courses_sheet, get_requests_per_course, 7, 2)

    tutors_sheet = wb.create_sheet(title='Ambassador')
    tutors_sheet.cell(column=1, row=1, value="Sessions Per Ambassador")
    insert_row(tutors_sheet, get_sessions_by_tutor, 1, 2)

    tutors_sheet.cell(column=4, row=1, value="# Courses Ambassador Can Tutor")
    insert_row(tutors_sheet, get_courses_by_tutor, 4, 2)

    tutors_sheet.cell(column=9, row=1, value="Surveys Per Ambassador")
    tutors_sheet.cell(column=9, row=2, value="Ambassador")
    tutors_sheet.cell(column=10, row=2, value="Total Surveys Expected")
    tutors_sheet.cell(column=11, row=2, value="Total Complete")
    insert_row(tutors_sheet, get_surveys_by_tutor, 9, 3)

    availability_sheet = wb.create_sheet(title='Availability')

    availability_sheet.cell(column=1, row=1, value="Free Hours vs Scheduled Hours By Ambassador")
    availability_sheet.cell(column=1, row=2, value="Ambassador")
    availability_sheet.cell(column=2, row=2, value="Free Hours")
    availability_sheet.cell(column=3, row=2, value="Scheduled Hours")
    insert_row(availability_sheet, get_hours_by_tutor, 1, 3)

    tutee_sheet = wb.create_sheet(title='Tutees')
    tutee_sheet.cell(column=1, row=1, value="Sessions Per Tutee")
    tutee_sheet.cell(column=1, row=2, value="Tutee")
    tutee_sheet.cell(column=2, row=2, value="# of Sessions")
    insert_row(tutee_sheet, get_sessions_per_tutee, 1, 3)

    tutee_sheet.cell(column=4, row=1, value="Requests Per Tutee")
    tutee_sheet.cell(column=4, row=2, value="Tutee")
    tutee_sheet.cell(column=5, row=2, value="# of Requests")
    insert_row(tutee_sheet, get_requests_per_tutee, 4, 3)


    tutee_sheet.cell(column=9, row=1, value="Surveys By Tutee")
    tutee_sheet.cell(column=9, row=2, value="Tutee")
    tutee_sheet.cell(column=10, row=2, value="Expected Surveys")
    tutee_sheet.cell(column=11, row=2, value="Completed Surveys")
    insert_row(tutee_sheet, get_surveys_by_tutee, 9,3)



    wb.save(filename=FNAME)

def get_requests_per_tutee():
    for user in ALL_USERS:
        requests = ALL_REQUESTS.filter(submitted_by=user)
        if requests:
            yield("{}".format(user.get_full_name().title()),
                    requests.count())

def get_sessions_per_tutee():
    for user in ALL_USERS:
        tut_sess = ALL_SESSIONS.filter(tutee=user, session_canceled=False)
        if tut_sess:
            yield("{}".format(user.get_full_name().title()),
                    tut_sess.count())

def get_hours_by_tutor():
    tutors = ALL_USERS.filter(is_tutor=True)
    availabilities = Availability.objects.all()

    for tutor in tutors:
        available = availabilities.filter(
                ambassador=tutor,
                can_schedule=True,
                is_scheduled=False
                ).count()
        scheduled = availabilities.filter(
                ambassador=tutor,
                is_scheduled=True
                ).count()
        yield( '{}'.format(tutor.get_full_name().title()),
                available,
                scheduled
                )

def get_surveys_by_tutee():
    surveys = TuteeSurvey.objects.filter(session_date__lte=TODAY)
    for user in ALL_USERS:
        all_survs = surveys.filter(tutoring_session__tutee=user)
        if all_survs:
            yield ("{}".format(
                user.get_full_name().title()),
                all_survs.count(),
                all_survs.filter(submitted=True).count())

def get_surveys_by_tutor():
    tutors = ALL_USERS.filter(is_tutor=True)
    surveys = AmbassadorSurvey.objects.filter(session_date__lte=TODAY)
    for tutor in tutors:
        all_survs = surveys.filter(tutoring_session__ambassador=tutor)
        if all_survs:
            yield ("{}".format(
                tutor.get_full_name().title()),
                all_survs.count(),
                all_survs.filter(submitted=True).count())


def get_courses_by_tutor():
    tutors = ALL_USERS.filter(is_tutor=True)
    for tutor in tutors:
        yield ('{}'.format(tutor.get_full_name().title()),
                tutor.courses_can_tutor.count())

def get_requests_per_course():
    everything = ALL_REQUESTS
    all_courses = Course.objects.all()
    for course in all_courses:
        yield ('{}'.format(course), everything.filter(course=course).count())

def get_sessions_per_course():
    everything = ALL_SESSIONS.filter(session_canceled=False)
    all_courses = Course.objects.all()
    for course in all_courses:
        yield ('{}'.format(course), everything.filter(course=course).count())

def get_tutors_per_course():
    all_courses = Course.objects.all()
    all_tutors = ALL_USERS.filter(is_tutor=True)
    for course in all_courses:
        yield ('{}'.format(course), all_tutors.filter(courses_can_tutor=course).count())


def get_sessions_by_tutor():
    everything = ALL_SESSIONS
    tutors = ALL_USERS.filter(is_tutor=True).exclude(email='admin@fiu.edu')
    for tutor in tutors:
        sessions = everything.filter(
                session_canceled=False,
                ambassador=tutor)
        session_count = sessions.count()       
        yield('{}'.format(tutor.get_full_name().title()),
                session_count)

def get_request_stats(start_date=SEVEN_DAYS_BEFORE, end_date=TODAY):
    everything = ALL_REQUESTS
    pending = everything.filter(status='A').count()
    print("Total pending requests: {}".format(pending))
    courses = Course.objects.all()
    for course in courses:
        requests = everything.filter(
                submitted_date__range=[start_date, end_date],
                course=course,
                status='A'
                ).count()
        print("{} requests for {}".format(requests, course))

