from custom_users.models import User
from tutoring_requests.models import TutoringRequest
from tutoring_sessions.models import TutoringSession
from availabilities.models import Availability
from surveys.models import AmbassadorSurvey, TuteeSurvey

class Totals:
    def __init__(self, today):
        self.users = User.objects.all().exclude(email='admin@fiu.edu')
        self.avas = Availability.objects.all()
        self.requests = TutoringRequest.objects.all()
        self.sessions = TutoringSession.objects.all()
        self.amb_survs = AmbassadorSurvey.objects.filter(session_date__lte=today)
        self.tut_survs = TuteeSurvey.objects.filter(session_date__lte=today)

    def get_session_totals(self):
        canceled = self.sessions.filter(session_canceled=True).count()
        on_going = self.sessions.filter(session_canceled=False).count()
        yield ('Canceled Sessions', canceled)
        yield ('On Going Sessions', on_going)

    def get_total_requests(self):
        pending = self.requests.filter(status='A').count()
        scheduled = self.requests.filter(status='B').count()
        canceled = self.requests.filter(status='C').count()
        yield ('Pending Requests', pending)
        yield ('Scheduled Requests', scheduled)
        yield ('Canceled Requests', canceled)


    def get_user_stats(self):
        total_users = self.users.count()
        tutors = self.users.filter(is_tutor=True).count()
        yield ("Total Users",total_users)
        yield ("Total Tutors", tutors)

    def get_availability_numbers(self):
        scheduled = self.avas.filter(is_scheduled=True).count()
        available = self.avas.filter(can_schedule=True, is_scheduled=False).count()
        yield ("Total hours scheduled", scheduled)
        yield ("Total hours available", available)
    
    def get_ambassador_survey_totals(self):
        yield ("Total Surveys", self.amb_survs.count())
        yield ("Completed", self.amb_survs.filter(submitted=True).count())

    def get_tutee_survey_totals(self):
        yield ("Total Surveys", self.tut_survs.count())
        yield ("Completed", self.tut_survs.filter(submitted=True).count())
