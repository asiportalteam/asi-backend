from django.db import models

# Create your models here.

TERMS = (
            ('FA', 'Fall'),
            ('SP', 'Spring'),
            ('SU', 'Summer'),
        )

class Semester(models.Model):
    start_date = models.DateField()
    end_date = models.DateField()
    term_type = models.CharField(choices=TERMS, default='SP', max_length=2)
    year = models.CharField(max_length=4, default="1971")

    def __str__(self):
        return "{} {}".format(self.get_term_type_display(), self.year)
