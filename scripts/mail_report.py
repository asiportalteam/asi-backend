from django.core.mail import EmailMessage
from django.utils import timezone
from reports.services import populate_book

def run(mailto):
    populate_book()
    now = timezone.now().date().strftime('%b %d %Y')
    fileout = timezone.now().date().strftime('%b_%d_%Y')
    email = EmailMessage(
            '[ASI] Report for week of {}'.format(now),
            'See attachments',
            'asi-noreply@cs.fiu.edu',
            [mailto],
            bcc=['jlope590@fiu.edu'],
            )
    email.attach_file('{}-report.xlsx'.format(timezone.now().date()))
    email.send()
