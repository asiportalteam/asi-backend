from tutoring_sessions.models import TutoringSession
from tutoring_requests.models import TutoringRequest
from surveys.models import AmbassadorSurvey, TuteeSurvey
from django.utils import timezone


def run():
    active_sessions = TutoringSession.objects.filter(
        session_canceled=False
    )

    pending_requests = TutoringRequest.objects.filter(
        status='A'
    )

    # deletes all active sessions
    for sess in active_sessions:
        # cancels session and sets end_date
        sess.session_canceled = True
        sess.end_date = timezone.now() - timezone.timedelta(days=4)
        sess.save()

        # resets availabilities
        ambassador = sess.ambassador
        availability_time = sess.start_time
        availability_day = sess.day
        actual_availability = ambassador.availabilities.get(start_time=availability_time, day=availability_day)
        actual_availability.can_schedule = True
        actual_availability.is_scheduled = False
        actual_availability.save()

        # deletes surveys
        created_at = timezone.now()
        ambassador_survey = AmbassadorSurvey.objects.filter(tutoring_session=sess, session_date__gt=created_at)
        tutee_survey = TuteeSurvey.objects.filter(tutoring_session=sess, session_date__gt=created_at)

        for every_survey in ambassador_survey:
            every_survey.delete()

        for every_survey in tutee_survey:
            every_survey.delete()

    # denies all pending requests
    for request in pending_requests:
        request.status = 'C'
        request.save()
