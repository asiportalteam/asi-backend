from availabilities.models import Availability
from courses.models import Course
from custom_users.models import User
import random

email_suffix = '@fiu.edu'
F_FULL = {
        'a' : 'Ash',
        'b' : 'Bash',
        'c' : 'Cash',
        'd' : 'Dash',
        'e' : 'East',
        'f' : 'Feast',
        'g' : 'Gunch',
        'h' : 'Heston',
        'i' : 'Ionian',
        'j' : 'Jonian',
        'k' : 'Konan',
        'l' : 'Lorem',
        'm' : 'Morem',
        'n' : 'Norem',
        'o' : 'Oral',
        'p' : 'Porque',
        'q' : 'Quinoa',
        'r' : 'Ridge',
        's' : 'Smidge',
        't' : 'Taint',
        'u' : 'Uranus',
        'v' : 'Vestibule',
        'w' : 'West',
        'x' : 'Xylophone',
        'y' : 'Yuengling',
        'z' : 'Zed',
        }
l_names = ['jone', 'lope', 'kope', 'diaz', 'gonz', 'garc', 'elia', 'pujo']

def gen_email():
    numz = '{0}{1}{2}'.format(
            random.randint(0,9),
            random.randint(0,9),
            random.randint(0,9),
            )
    return "{0}{1}{2}{3}".format(
            chr(random.randint(97,122)),
            l_names[random.randint(0, len(l_names) - 1)],
            numz,
            email_suffix,
            )

def run():
    # make superuser
    u1 = User.objects.create(
            email = 'admin@fiu.edu',
            username = 'admin@fiu.edu',
            first_name = 'Asiad',
            last_name = 'Min',
            is_superuser=True,
            is_tutor = True,
            is_team_leader = True,
            is_logistic_director = True,
            is_staff=True
            )
    u1.set_password('passw0rd')
    u1.save()
    
    c1 = Course.objects.create(
            subject = 'COP',
            number = '2210',
            full_name = 'Programming 1',
            professor = 'W. Fields',
            )
    c2 = Course.objects.create(
            subject = 'MAD',
            number = '1100',
            full_name = 'Discreete',
            professor = 'Gormy',
            )
    c3 = Course.objects.create(
            subject = 'CDA',
            number = '3103',
            full_name = 'Fundamentals of Computer',
            professor = 'Pestainer',
            )

    courses = [c1, c2, c3]

    for x in range(3):
        uname = gen_email()
        print("Creating ambassador with email : {0}".format(uname))
        amb = User.objects.create(
                email = uname,
                username = uname,
                first_name = F_FULL.get(uname[0]),
                last_name = uname[1:5],
                is_tutor=True,
                )
        amb.set_password('password')
        course = courses[random.randint(0,2)]
        print("Adding {0} to {1} courses".format(course, amb))
        amb.courses_can_tutor.add(course)
        amb.save()
        avas = Availability.make_availabilities(amb)
        for ava in avas:
            if random.randint(0, 100) % 5 == 0:
                ava.can_schedule = True
                ava.save()
