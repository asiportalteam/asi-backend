from django.utils import timezone
from django.core.mail import EmailMessage
from datetime import timedelta
from custom_users.models import User
from timesheets.services import prepare_timesheets, zip_em_all

def run(mailto, start_date=None):
    if start_date is None:
        tutors = User.objects.filter(is_tutor=True)
        for tutor in tutors:
            d_range = [(timezone.now() + timedelta(days=-13)).date(), 
                    timezone.now().date()]
            prepare_timesheets(tutor, d_range[0], d_range[1])
        output_name = zip_em_all()
    else:
        tutors = User.objects.filter(is_tutor=True)
        for tutor in tutors:
            d_range = [start_date, start_date + timedelta(weeks=2)]
            prepare_timesheets(tutor, d_range[0], d_range[1])
        output_name = zip_em_all()
    email = EmailMessage(
            '[ASI] Timesheets for {} - {}'.format(
                d_range[0].strftime('%b %d'),
                d_range[1].strftime('%b %d'),
                ),
            'Timesheets are attached in zip file.',
            'asi-noreply@cs.fiu.edu',
            [mailto],
            bcc=['jlope590@fiu.edu']
            )
    email.attach_file(output_name)
    email.send()
