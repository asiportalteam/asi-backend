from custom_users.models import User
from timesheets.services import prepare_timesheets, zip_em_all

def run(start_date, end_date):
    tutors = User.objects.filter(is_tutor=True)
    for tutor in tutors:
        prepare_timesheets(tutor, start_date, end_date)
    zip_em_all()
