from tutoring_requests.models import TutoringRequest
from tutoring_sessions.models import TutoringSession


def run():
    panther_ids = list()
    emails = list()

    active_sessions = TutoringSession.objects.filter(
        session_canceled=False
    )

    pending_requests = TutoringRequest.objects.filter(
        status='A'
    )

    for sess in active_sessions:
        panther_ids.append(sess.tutee.panther_id)
        emails.append(sess.tutee)

    for request in pending_requests:
        panther_ids.append(request.submitted_by.panther_id)
        emails.append(request.submitted_by)

    panther_ids = remove_duplicates(panther_ids)
    emails = remove_duplicates(emails)

    with open("tutee_PIDs.csv", "w") as f:
        for index in range(len(panther_ids)):
            f.write(f"{panther_ids[index]},{emails[index]}\n")


def remove_duplicates(input_list):
    return list(dict.fromkeys(input_list))
