from time import sleep
from emailer.mailer import send_email_blast
from tutoring_sessions.models import TutoringSession
from tutoring_requests.models import TutoringRequest


def run():

    active_sessions = TutoringSession.objects.filter(
        session_canceled=False,
        day=4
    )

    for sess in active_sessions:
        sleep(5)
        send_email_blast(sess)
