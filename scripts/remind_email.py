from datetime import timedelta
from time import sleep
from django.utils import timezone
from emailer.mailer import send_tutoring_reminder
from tutoring_sessions.models import TutoringSession


def run():
    now = timezone.now().date()
    tomorrow = now + timedelta(days=1)
    tomorrow_sessions = TutoringSession.objects.filter(
        start_date=tomorrow,
        session_canceled=False
    )
    for sess in tomorrow_sessions:
        sleep(2)
        send_tutoring_reminder(sess)
