'''
Takes care of all emails that need to be sent by the backend application
'''
from django.core.mail import EmailMessage
from django.template.loader import get_template


def base_prepare(title, template, email_to, context):
    '''
    Function that prepares the majority of the message to enforce DRY principals
    '''
    title = title
    email_template = get_template(template)
    message = email_template.render(context)
    email = EmailMessage(
        title,
        message,
        'asi-noreply@cs.fiu.edu',
        email_to,
        bcc=['asisoftwaretest@gmail.com']
    )
    email.content_subtype = 'html'
    return email


def send_tutoring_reminder(session):
    '''
    Script that will be run everyday at 3:30 pm to remind people that they've got tutoring sessions.
    '''
    title = '[ASI] Reminder! Your First Tutoring Session Is Tomorrow'
    email_context = {
        'first_name': session.tutee.first_name,
        'ambassador': session.ambassador.get_full_name(),
        'course': session.course,
        'date': session.start_date,
        'time': session.get_american_time(),
        'amb_email': session.ambassador.email
    }
    email = base_prepare(
        title, 'emailer/reminder.html', [session.tutee.email,
                                         session.ambassador.email],
        email_context
    )
    email.send()


def send_account_confirmation_code(user):
    '''
    Email which is sent when a user registers for a new account.
    '''
    title = '[ASI] Please Activate Your Account'
    email_context = {
        'full_name': user.get_full_name(),
        'username': user.email,
        'code': user.activation_code,
    }
    email = base_prepare(
        title, 'emailer/activation_code.html', [user.email], email_context)
    email.send()


def send_password_reset_code(user):
    '''
    Email which is sent when a user registers for a new account.
    '''
    title = '[ASI] Password Reset Code'
    email_context = {
        'full_name': user.get_full_name(),
        'username': user.email,
        'code': user.activation_code,
    }
    email = base_prepare(
        title, 'emailer/password_reset_code.html', [user.email], email_context)
    email.send()


def send_tutoring_request_confirmation(user, request):
    '''
    NOT FUCKING SESSION SCHEDULED OCNFIRMATIONSEOJFOSIEJFOJSF
    '''
    title = '[ASI] We Got Your Tutoring Request'
    email_context = {
        'first_name': user.first_name,
        'request': request
    }
    email = base_prepare(
        title, 'emailer/request_confirm.html', [user.email], email_context)
    email.send()


def send_tutoring_session_scheduled(session):
    '''
    Email sent when request is finally scheduled
    '''
    title = '[ASI] We Just Scheduled Your Tutoring Session'
    email_context = {
        'course': session.course,
        'ambassador': session.ambassador.get_full_name(),
        'time': session.start_time,
        'day': session.get_day_display(),
        'start_date': session.start_date,
        'amb_email': session.ambassador.email,
        'end_date': session.end_date,
    }
    email = base_prepare(
        title, 'emailer/session_scheduled.html',
        [session.tutee.email, session.ambassador.email],
        email_context)
    email.send()


def send_no_rooms(request):
    '''
    Email sent when request is marked as no rooms available
    '''

    title = "[ASI] Tutoring Request Unable to be Fulfilled"
    email_context = {
        'full_name': request.submitted_by.get_full_name(),
        'request': request
    }
    email = base_prepare(
        title, 'emailer/no_rooms.html',
        [request.submitted_by.email],
        email_context)
    email.send()


def send_request_denied(request):
    '''
    Email sent when request is denied
    '''

    title = "[ASI] Tutoring Request Unable to be Fulfilled"
    email_context = {
        'full_name': request.submitted_by.get_full_name(),
        'request': request,
        'course': request.course
    }
    email = base_prepare(
        title, 'emailer/request_denied.html',
        [request.submitted_by.email],
        email_context)
    email.send()


def cancellation_processed(request):
    '''
        Email sent when request is marked as processed in cancellation table
        '''

    title = "[ASI] Tutoring Session Has Been Canceled"
    email_context = {
        'tutoring_session': request.tutoring_session,
        'request': request
    }
    email = base_prepare(
        title, 'emailer/confirm_cancellation.html',
        [request.tutoring_session.ambassador.email, request.tutoring_session.tutee.email],
        email_context)
    email.send()


def cancellation_created(request):
    '''
        Email sent when request is marked as processed in cancellation table
        '''

    title = "[ASI] Tutoring Session Cancellation Request RECEIVED"
    email_context = {
        'tutoring_session': request.tutoring_session,
        'request': request
    }
    email = base_prepare(
        title, 'emailer/cancellation_created.html',
        [request.tutoring_session.ambassador.email, request.tutoring_session.tutee.email],
        email_context)
    email.send()


def send_unexcused_absence(session, absence_date):
    '''
    Email which is sent when a tutee has an unexcused absence.
    '''
    title = '[ASI] Tutoring Session Unexcused Absence'
    email_context = {
        'first_name': session.tutee.first_name,
        'ambassador': session.ambassador.get_full_name(),
        'course': session.course,
        'day': session.get_day_display(),
        'time': session.get_american_time(),
        'date': absence_date,
        'amb_email': session.ambassador.email
    }
    email = base_prepare(
        title, 'emailer/unexcused_absence.html',
        [session.tutee.email, session.ambassador.email],
        email_context)
    email.send()


def send_email_blast(session):
    '''
    Email blast sent to all tutees and tutors.
    '''
    title = '[ASI] No Tutoring on Veterans Day'
    email_context = {
        'first_name': session.tutee.first_name,
        'ambassador': session.ambassador.get_full_name(),
        'course': session.course,
        'day': session.get_day_display(),
        'time': session.get_american_time(),
    }
    email = base_prepare(
        title, 'emailer/email_blast.html',
        [session.tutee.email, session.ambassador.email], email_context)
    email.send()


def send_reminder(session):
    title = '[ASI] Session Reminder'
    email_context = {'first_name': session.tutee.first_name, 'course': session.course,
                     'ambassador': session.ambassador.get_full_name(), 'time': session.get_american_time(),
                     'amb_email': session.ambassador.email, 'zoom_pmi': session.ambassador.zoom_pmi,
                     }
    email = base_prepare(
        title, 'emailer/session_reminder.html', [session.tutee.email], email_context
    )
    email.send()
