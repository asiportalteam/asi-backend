# External API Usage
There is an external running version of this API located at:

[api.wereally.online](http://api.wereally.online "API Link")

With the use of Swagger UI, all endpoints can be viewed and tested. Before you can have access to all endpoints, 
you must first retrieve a token from the /api/accounts/login/.

For the sake of simplicity there is a user account which has every possible permssion. To retireve their token log in with the credentials :

email : admin@asi.com
password : passw0rd


You should get back a token similar to this:

~~~
{
"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJ1c2VybmFtZSI6ImFkbWluQGFzaS5jb20iLCJleHAiOjE1MTgwOTc4MTgsImVtYWlsIjoiYWRtaW5AYXNpLmNvbSJ9.gJJNf4Fdmo9A9cM9tvJBbAPKbmR-S7w8XWbLJ_aaN5M"
}
~~~
Once you retrieve this token you can now view the rest of the API endpoints on the Swagger interface by going to the top right corner and clicking the Authorize button.


in the value box paste the token you recieved prepended with JWT it should look like this : 

    jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJ1c2VybmFtZSI6ImFkbWluQGFzaS5jb20iLCJleHAiOjE1MTgwOTc4MTgsImVtYWlsIjoiYWRtaW5AYXNpLmNvbSJ9.gJJNf4Fdmo9A9cM9tvJBbAPKbmR-S7w8XWbLJ_aaN5M
