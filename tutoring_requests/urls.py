from django.conf.urls import url
from tutoring_requests import views

urlpatterns = [
    url(r'^create/?$', views.TutoringRequestCreateView.as_view(), name='create'),
    url(r'^update/(?P<pk>[0-9]+)/$',
        views.TutoringRequestUpdateView.as_view(), name='update'),
    url(r'^list/?$', views.TutoringRequestListView.as_view()),
]
