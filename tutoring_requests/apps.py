from django.apps import AppConfig


class TutoringRequestsConfig(AppConfig):
    name = 'tutoring_requests'
