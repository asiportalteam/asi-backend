from django.shortcuts import render
from rest_framework.views import APIView
from custom_permissions import permissions as cp
from rest_framework import status, generics, permissions
from rest_framework.response import Response
from tutoring_sessions.models import TutoringSession
from surveys.models import AmbassadorSurvey, TuteeSurvey
from emailer.mailer import send_tutoring_request_confirmation, send_tutoring_session_scheduled, send_no_rooms, send_request_denied
from .models import TutoringRequest
from cancelation_requests.models import CancelationRequest
from cancelation_requests.serializers import CancellationRequestsSerializer
from .serializers import (
    CreateTutoringRequestSerializer,
    UpdateTutoringRequestSerializer,
    ListTutoringRequestSerializer)
from availabilities.models import Availability


def no_other_exists(availability):
    find = TutoringRequest.objects.filter(
        availability=availability,
        status='A')
    return find.count() == 0


class TutoringRequestCreateView(generics.GenericAPIView):
    """
    Endpoint for users to create tutoring requests.
    """
    serializer_class = CreateTutoringRequestSerializer

    def post(self, request):
        '''
        Need to do some brokering i.e. shoot an email their way
        '''
        pending_request = {}
        pending_request['request'] = request
        pending_request['availability'] = request.data.get('availability', {})
        pending_request['course']= request.data.get('course', {})
        course = pending_request['course']
        user_requests = TutoringRequest.objects.filter(status='A', submitted_by=request.user)
        availability = Availability.objects.get(id=pending_request['availability'])

        user_requests_count = user_requests.count() + TutoringSession.objects.filter(session_canceled=False, tutee=request.user).count()

        if request.user.request_limit < user_requests_count:
            content = {
                'msg': "You have either have too many pending tutoring requests, scheduled sessions, or a combination of the two. Please contact us for more help.",
                'msg_type': 'ERROR'
            }
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

        if user_requests.filter(availability__day=availability.day,
                                availability__start_time=availability.start_time).exists():
            content = {
                'msg': "You already have a pending session for this day and time.",
                'msg_type': 'ERROR'
            }
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

        if request.user.sessions_per_course <= (len(user_requests.filter(course=course,status='A')) + len(TutoringSession.objects.all().filter(tutee=request.user,course=course,session_canceled=False))):
            content = {
                'msg': "You have too many active/pending sessions for this course.",
                'msg_type': 'ERROR'
            }
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

        if no_other_exists(pending_request['availability']):
            serializer = self.serializer_class(
                data=pending_request, context={'request': request})
            serializer.is_valid(raise_exception=True)
            created = serializer.save()

            tutoring_sess = TutoringSession.create_from_request(created)
            tutoring_sess.save()
            send_tutoring_session_scheduled(tutoring_sess)
            # Create our surveys
            AmbassadorSurvey.create_from_session(tutoring_sess)
            TuteeSurvey.create_from_session(tutoring_sess)

            created.availability.is_scheduled = True
            created.availability.save()

            created.status = 'B'
            created.save()

            content = {
                'msg': "Your request was successfully scheduled! Please check your email for more info.",
                'msg_type': 'SUCCESS'
            }
            return Response(content, status=status.HTTP_201_CREATED)

        else:
            content = {
                'msg': 'Whoops! Someone else beat you to this time slot! Please try a different one.',
                'msg_type': 'ERROR',
            }
            return Response(content, status=status.HTTP_400_BAD_REQUEST)


class TutoringRequestListView(generics.GenericAPIView):
    """
    End point for logistic director to see all pending requests.
    """
    permission_classes = (permissions.IsAuthenticated, cp.IsLogisticDirector)
    queryset = TutoringRequest.objects.all()
    serializer_class = ListTutoringRequestSerializer

    def get(self, request):
        data = self.queryset.filter(status = 'A').order_by('status', 'submitted_date')
        cancel_data = CancelationRequest.objects.filter(status = 'D').order_by('created_at')
        cancel_content = CancellationRequestsSerializer(cancel_data, many = True)
        content = self.serializer_class(data, many=True)
        bigList = content.data + cancel_content.data
        return Response(bigList, status=status.HTTP_200_OK)


class TutoringRequestUpdateView(generics.GenericAPIView):
    '''
    End point for logistic director to update a specific request
    '''
    permission_classes = (permissions.IsAuthenticated, cp.IsLogisticDirector)
    serializer_class = UpdateTutoringRequestSerializer

    def patch(self, request, pk):
        updated = dict()
        updated['status'] = request.data.get('status')
        tr = TutoringRequest.objects.get(pk=pk)
        serializer = UpdateTutoringRequestSerializer(tr, data=updated)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        # create our tutoring session
        if updated['status'] == 'B':
            tutoring_sess = TutoringSession.create_from_request(tr)
            tutoring_sess.save()
            send_tutoring_session_scheduled(tutoring_sess)
            # Create our surveys
            AmbassadorSurvey.create_from_session(tutoring_sess)
            TuteeSurvey.create_from_session(tutoring_sess)
            _debug = {
                'str': tutoring_sess.__str__(),
                'id': tutoring_sess.id,
                'start': tutoring_sess.start_date,
                'end': tutoring_sess.end_date,
            }
            return Response({
                'msg': 'Successfully scheduled session.',
                'msg_type': 'SUCCESS'
            }, status=status.HTTP_201_CREATED)
        elif updated['status'] == 'C':
            send_request_denied(tr)
            return Response({
                'msg': 'Canceled tutoring request'
            })
        elif updated['status'] == 'D':
            send_no_rooms(tr)
            return Response({
                'msg': 'Request updated. Student notified.'
            })
