from rest_framework import serializers
from .models import TutoringRequest, STATUS_CODES

class CreateTutoringRequestSerializer(serializers.ModelSerializer):
    submitted_by = serializers.PrimaryKeyRelatedField(
            read_only=True,
            default=serializers.CurrentUserDefault(),
            )
    class Meta:
        model = TutoringRequest
        fields = ['submitted_by', 'availability', 'course']

class UpdateTutoringRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TutoringRequest
        fields = ['status', 'room']

class ListTutoringRequestSerializer(serializers.ModelSerializer):
    submitted_by = serializers.ReadOnlyField(
            source = 'submitted_by.get_full_name'
            )
    ambassador = serializers.CharField(
            source = 'availability.ambassador.get_full_name',
            read_only = True
            )
    time = serializers.CharField(
            source = 'availability.get_anonymous_time',
            read_only = True
            )
    course = serializers.ReadOnlyField(
            source = 'course.__str__',
            )
    status = serializers.ReadOnlyField()
    room = serializers.ReadOnlyField()
    class Meta:
        model = TutoringRequest
        fields = ['id', 'submitted_by', 'course', 'status', 'ambassador', 'time', 'room']
        read_only_fields = ('id', 'submitted_by', 'availability', 'course', 'status')
