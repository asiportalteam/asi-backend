from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from courses.models import Course
from .models import TutoringRequest


class OfferedCourseOnlyFilter(admin.SimpleListFilter):
    title = _('course')
    parameter_name = 'course'

    def lookups(self, request, model_admin):
        courses = Course.objects.filter(offered=True)
        return (
            (course.id, _(course.subject+course.number)) for course in courses)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(course=self.value())
        else:
            return queryset


# Register your models here.

class TutoringRequestAdmin(admin.ModelAdmin):
    list_display = ['course', 'whomst', 'email', 'availability', 'submitted_date', 'status']
    list_filter = ['status', OfferedCourseOnlyFilter, 'submitted_by']

    def whomst(self, obj):
        return obj.submitted_by.get_full_name()

    def email(self, obj):
        return obj.submitted_by.email


admin.site.register(TutoringRequest, TutoringRequestAdmin)
