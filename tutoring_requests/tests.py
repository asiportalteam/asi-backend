import datetime
from unittest.mock import patch
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase
from .models import TutoringRequest
from asi_api.settings import MIN_DAYS_TO_SCHEDULE
from availabilities.models import Availability
from tutoring_sessions.models import TutoringSession
from surveys.models import AmbassadorSurvey, TuteeSurvey
from courses.models import Course
from custom_users.models import User

class TutoringRequestSetUp(TestCase):
    def setUp(self):
        self.course = Course.objects.create(
                subject='MAC',
                number='2312',
                )
        self.amb = User.objects.create(
                username='somedude@fiu.edu',
                email='somedude@fiu.edu',
                first_name='Some',
                last_name='Dude',
                is_logistic_director=True,
                is_tutor=True,
                )
        self.amb.courses_can_tutor.add(self.course)
        self.amb.save()
        Availability.make_availabilities(self.amb)
        self.user = User.objects.create(
                username='joblow@fiu.edu',
                email='joblow@fiu.edu',
                first_name='Joe',
                last_name='Blow',
                )
        self.jwt_token = self.user.token

class TestTutoringRequestModel(TutoringRequestSetUp):
    ''' 
    Gonna test that when we save a tutoring request : 
    1 . we send a signal to the Availability that it needs to be updated
    '''
    def test_that_on_creation_availability_gets_marked_as_scheduled(self):
        ava = Availability.objects.filter(ambassador = self.amb)[0]
        TutoringRequest.objects.create(
                submitted_by=self.user,
                course = self.course,
                availability = ava
                )
        ava2 = Availability.objects.get(pk = ava.pk)
        self.assertTrue(ava2.is_scheduled)

    def test_that_on_scheduling_availability_gets_marked_as_scheduled(self):
        ava = Availability.objects.filter(ambassador = self.amb)[0]
        tr = TutoringRequest.objects.create(
                submitted_by=self.user,
                course = self.course,
                availability = ava
                )
        ava2 = Availability.objects.get(pk = ava.pk)
        self.assertTrue(ava2.is_scheduled)
        tr.status = 'B'
        tr.save()
        ava2 = Availability.objects.get(pk = ava.pk)
        self.assertTrue(ava2.is_scheduled)

    def test_that_on_cancel_session_avail_gets_marked_as_unscheduled(self):
        ava = Availability.objects.filter(ambassador = self.amb)[0]
        tr = TutoringRequest.objects.create(
                submitted_by=self.user,
                course = self.course,
                availability = ava
                )
        ava2 = Availability.objects.get(pk = ava.pk)
        self.assertTrue(ava2.is_scheduled)
        tr.status = 'C'
        tr.save()
        ava2 = Availability.objects.get(pk = ava.pk)
        self.assertFalse(ava2.is_scheduled)

    def test_that_on_no_room_avail_gets_marked_as_free(self):
        ava = Availability.objects.filter(ambassador = self.amb)[0]
        tr = TutoringRequest.objects.create(
                submitted_by=self.user,
                course = self.course,
                availability = ava
                )
        ava2 = Availability.objects.get(pk = ava.pk)
        self.assertTrue(ava2.is_scheduled)
        tr.status = 'D'
        tr.save()
        ava2 = Availability.objects.get(pk = ava.pk)
        self.assertFalse(ava2.is_scheduled)

class TestTutoringRequestCreateView(TutoringRequestSetUp, APITestCase):
    '''
    Gonna test for a few key features of creating a tutoring request.
    1. The # of requests that a user has open is correctly changed.
    2. We can't create multiple requests for the same time, day , and ambassador.
    '''
    def test_that_we_can_make_tutoring_requests(self):
        url = reverse('tutoring_requests:create')
        availability = Availability.objects.filter(ambassador=self.amb)
        data = { 'course' : self.course.pk,
                 'availability' : availability[0].pk
                 }
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['_errors'], None)

    def test_that_we_cant_submit_two_requests_for_one_availability(self):
        '''
        A user shouldn't be able to make two requests for the same availability
        '''
        url = reverse('tutoring_requests:create')
        availability = Availability.objects.filter(ambassador=self.amb)
        data = { 'course' : self.course.pk,
                 'availability' : availability[0].pk
                 }
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['_errors'], None)
        data = { 'course' : self.course.pk,
                 'availability' : availability[0].pk
                 }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['_errors'], ['There is already a request for that time.'])

    def test_that_users_past_limit_cant_make_requests(self):
        self.user.request_limit = 0
        self.user.save()
        url = reverse('tutoring_requests:create')
        availability = Availability.objects.filter(ambassador=self.amb)
        data = { 'course' : self.course.pk,
                 'availability' : availability[0].pk
                 }
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.jwt_token))
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['_errors'], 'You have too many pending requests.')

class TestTutoringRequestUpdate(TutoringRequestSetUp, APITestCase):
    def setUp(self):
        super(TestTutoringRequestUpdate, self).setUp()
        self.ava = Availability.objects.filter(ambassador=self.amb)[0]
        self.request = TutoringRequest.objects.create(
                submitted_by = self.user,
                availability = self.ava,
                course = self.course,)
    
    @patch('tutoring_sessions.models.SEMESTER_END_DATE')
    def test_that_successful_updates(self, mocked_date):
        '''
        Assure that our session is getting saved and that there's an appropriate amt of surveys
        '''
        mocked_date = timezone.now() + datetime.timedelta(days=30)
        url = reverse('tutoring_requests:update', kwargs={'pk' : self.request.pk} )
        expected_start_date = timezone.now() + datetime.timedelta(days=abs(self.ava.day - timezone.now().date().isoweekday()))
        if abs(expected_start_date - timezone.now()).days > MIN_DAYS_TO_SCHEDULE:
            expected_start_date += datetime.timedelta(days=7)
        data = { 'status' : 'B' }
        self.client.credentials(HTTP_AUTHORIZATION='jwt {0}'.format(self.amb.token))
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['session']['start'], expected_start_date.date())
        session = TutoringSession.objects.get(id=response.data['session'][
            'id'])
        amb_survz = AmbassadorSurvey.objects.filter(tutoring_session=response.data['session']['id'])
        self.assertEqual(amb_survz.count(), len(session.get_dates()))
        tut_survz = TuteeSurvey.objects.filter(tutoring_session=response.data['session']['id'])
        self.assertEqual(tut_survz.count(), len(session.get_dates()))
