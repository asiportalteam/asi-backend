from django.db import models
from custom_users.models import User
from availabilities.models import Availability
from courses.models import Course

STATUS_CODES = (
    ('A', 'Pending'),
    ('B', 'Scheduled'),
    ('C', 'Canceled'),
    ('D', 'No Room Available'),
)

ROOMS = (
        ('101A', '101A'),
        ('101B', '101B'),
        ('101C', '101C'),
        ('101D', '101D'),
)


class TutoringRequest(models.Model):
    submitted_by = models.ForeignKey(User,
                                     related_name='tutoring_requests')
    submitted_date = models.DateTimeField(auto_now_add=True)
    availability = models.ForeignKey(Availability)
    course = models.ForeignKey(Course)
    room = models.CharField(choices=ROOMS, default='101A', max_length=4)
    status = models.CharField(
        choices=STATUS_CODES,
        default='A',
        max_length=1
    )

    def __str__(self):
        return "{} tutoring at {} on {}'s".format(
            self.course,
            self.availability.get_american_time(),
            self.availability.get_day_display(),
        )

    def save(self, *args, **kwargs):
        # TODO FIX THIS shit
        '''
        Let us reflect and think is it really okay for a model to adjust another model? I think not.
        Gonna think a bit more about this.
        '''
        if self.status in ['A', 'B']:
            self.availability.is_scheduled = True
            self.availability.save()
        elif self.status in ['C', 'D']:
            self.availability.is_scheduled = False
            self.availability.save()
        super(TutoringRequest, self).save(*args, **kwargs)
